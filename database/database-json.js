// ------------------------------------------------------------------- DATA
var REPAIR_TYPES = {
    "repairTypes": [
        {
            "phone": {
                "networks": [
                    "AT&T",
                    "Verizon",
                    "Sprint",
                    "T-Mobile",
                    "H2O",
                    "Cricket",
                    "Lycamobile",
                    "Boost Mobile",
                    "MetroPCS",
                    "Gosmart",
                    "Net 10",
                    "Straight Talk",
                    "US Cellular",
                    "Simple Mobile",
                    "Ultra Mobile",
                    "Google",
                    "Other"
                ],
                "issues": [
                    "Broken Screen",
                    "Battery",
                    "Liquid Damage",
                    "Won't Charge",
                    "Camera(s) Issue",
                    "Data Backup",
                    "Won't Turn On",
                    "Software Issue(s)",
                    "Audio Issue(s)",
                    "Button Issue(s)",
                    "Network Issue",
                    "Wifi/Bluetooth Issue",
                    "Other"
                ],
                "manufacturers": [
                    {
                        "name": "Apple",
                        "models": [
                            {
                                "model": "iPhone 7 Plus",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Jet Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 7",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Jet Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 6s Plus",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 6s",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 6 Plus",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 6",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone SE",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 5s",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 5c",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Pink",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Yellow",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Green",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 5",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 4s",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": [
                                        "Broken Back Glass"
                                    ]
                                }
                            },
                            {
                                "model": "iPhone 4",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": [
                                        "Broken Back Glass"
                                    ]
                                }
                            },
                            {
                                "model": "iPhone 3Gs",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 3G",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPhone 1st Gen",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Samsung",
                        "models": [
                            {
                                "model": "Galaxy S6",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black Sapphire",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold Platinum",
                                            "subtitle": "Gold Screen"
                                        },
                                        {
                                            "name": "White Pearl",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Blue Topaz",
                                            "subtitle": "Blue Screen"
                                        }
                                    ],
                                    "issue": [
                                        "Broken Back Glass"
                                    ]
                                }
                            },
                            {
                                "model": "Galaxy S6 Edge",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black Sapphire",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold Platinum",
                                            "subtitle": "Gold Screen"
                                        },
                                        {
                                            "name": "White Pearl",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Green Emerald",
                                            "subtitle": "Green Screen"
                                        }
                                    ],
                                    "issue": [
                                        "Broken Back Glass"
                                    ]
                                }
                            },
                            {
                                "model": "Galaxy S6 Active",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gray",
                                            "subtitle": "Gray Screen"
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": "Blue Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy S6 Edge+",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black Sapphire",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold Platinum",
                                            "subtitle": "Gold Screen"
                                        },
                                        {
                                            "name": "White Pearl",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver Titan",
                                            "subtitle": "Silver Screen"
                                        },
                                        {
                                            "name": "Pink Gold",
                                            "subtitle": "Pink Screen"
                                        }
                                    ],
                                    "issue": [
                                        "Broken Back Glass"
                                    ]
                                }
                            },
                            {
                                "model": "Galaxy S7",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Silver Titanium",
                                            "subtitle": "Silver Screen"
                                        },
                                        {
                                            "name": "Black Onyx",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold Platinum",
                                            "subtitle": "Gold Screen"
                                        },
                                        {
                                            "name": "White Pearl",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Pink Gold",
                                            "subtitle": "Pink Screen"
                                        }
                                    ],
                                    "issue": [
                                        "Broken Back Glass"
                                    ]
                                }
                            },
                            {
                                "model": "Galaxy S7 Edge",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Silver Titanium",
                                            "subtitle": "Silver Screen"
                                        },
                                        {
                                            "name": "Black Onyx",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold Platinum",
                                            "subtitle": "Gold Screen"
                                        },
                                        {
                                            "name": "White Pearl",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Pink Gold",
                                            "subtitle": "Pink Screen"
                                        },
                                        {
                                            "name": "Blue Coral",
                                            "subtitle": "Blue Screen"
                                        }
                                    ],
                                    "issue": [
                                        "Broken Back Glass"
                                    ]
                                }
                            },
                            {
                                "model": "Galaxy S7 Active",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Titanium Gray",
                                            "subtitle": "Gray Screen"
                                        },
                                        {
                                            "name": "Green Camo",
                                            "subtitle": "Green Screen"
                                        },
                                        {
                                            "name": "Sandy Gold",
                                            "subtitle": "Gold Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy S5",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black Sapphire",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold Platinum",
                                            "subtitle": "Gold Screen"
                                        },
                                        {
                                            "name": "White Pearl",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver Titanium",
                                            "subtitle": "Silver Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy S4",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Blue Arctic",
                                            "subtitle": "Blue Screen"
                                        },
                                        {
                                            "name": "Purple Mirage",
                                            "subtitle": "Purple Screen"
                                        },
                                        {
                                            "name": "Red Aurora",
                                            "subtitle": "Red Screen"
                                        },
                                        {
                                            "name": "Brown Autumn",
                                            "subtitle": "Brown Screen"
                                        },
                                        {
                                            "name": "Pink Twilight",
                                            "subtitle": "Pink Screen"
                                        },
                                        {
                                            "name": "Frost White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Mist Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy S3",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Amber Brown",
                                            "subtitle": "Brown Screen"
                                        },
                                        {
                                            "name": "Garnet Red",
                                            "subtitle": "Red Screen"
                                        },
                                        {
                                            "name": "Sapphire Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Titanium Gray",
                                            "subtitle": "Gray Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy Note 2",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Titanium Gray",
                                            "subtitle": "Gray Screen"
                                        },
                                        {
                                            "name": "Marble White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Amber Brown",
                                            "subtitle": "Brown Screen"
                                        },
                                        {
                                            "name": "Ruby Wine",
                                            "subtitle": "Wine Screen"
                                        },
                                        {
                                            "name": "Pink",
                                            "subtitle": "Pink Screen"
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": "Blue Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy Note 3",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Jet Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Classic White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Blush Pink",
                                            "subtitle": "Pink Screen"
                                        },
                                        {
                                            "name": "Merlot Red",
                                            "subtitle": "Red Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy Note 4",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Charcoal Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Frost White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Blossom Pink",
                                            "subtitle": "Pink Screen"
                                        },
                                        {
                                            "name": "Bronze Gold",
                                            "subtitle": "Gold Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy Note 5",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black Sapphire",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold Platinum",
                                            "subtitle": "Gold Screen"
                                        },
                                        {
                                            "name": "Silver Titanium",
                                            "subtitle": "Silver Screen"
                                        },
                                        {
                                            "name": "White Pearl",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": [
                                        "Broken Back Glass"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Back Glass"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Google",
                        "models": [
                            {
                                "model": "Pixel",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Very Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Quite Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Really Blue",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Pixel XL",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Very Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Quite Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Really Blue",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "LG",
                        "models": [
                            {
                                "model": "G2",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Red",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "G3",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "True Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Metallic Black",
                                            "subtitle": "Gray Screen"
                                        },
                                        {
                                            "name": "Silk White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Burgundy Red",
                                            "subtitle": "Burgundy Red Screen"
                                        },
                                        {
                                            "name": "Moon Violet",
                                            "subtitle": "Moon Violet Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "G4",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Brown",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Red",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Yellow",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Sky Blue",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Beige",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Grey",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White Gold",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "G5",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Titan",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Pink",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "V20",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Urban Gray",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Romantic Pink",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Sweet Silver",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "V10",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Space Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Opal Blue",
                                            "subtitle": "Blue Screen"
                                        },
                                        {
                                            "name": "Luxe White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Modern Beige",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Nexus 4",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Nexus 5",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Red",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Nexus 5x",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Carbon",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Quartz",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Ice",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Motorola",
                        "models": [
                            {
                                "model": "Moto Z",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Droid Razr HD / Maxx HD",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Droid Ultra / Maxx",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Droid Turbo 2 / Moto X Force",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Droid Turbo",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Droid Maxx 2",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Moto X",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Moto X Pure Edition",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Moto G",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Moto E",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "OnePlus",
                        "models": [
                            {
                                "model": "One",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "2",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "3",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Graphite",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Soft Gold",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "3T",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gunmetal",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Soft Gold",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "X",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Onyx",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Ceramic",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "HTC",
                        "models": [
                            {
                                "model": "One M7",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Red",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "One M8",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Amber Gold",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Glacial Silver",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gunmetal Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "One M9",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Amber Gold",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Silver/Rose Gold",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gunmetal Gray",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold/Pink",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Pink",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "One A9",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Carbon Gray",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Opal Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Topaz Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Deep Garnet",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Pink",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "10",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Carbon Gray",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Glacier Silver",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Topaz Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Camila Red",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Bolt",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gunmetal",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Glacier Silver",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Desire",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Sony",
                        "models": [
                            {
                                "model": "Xperia XZ",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "Silver Screen"
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": "Blue Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia X Compact",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": "Blue Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia XA Ultra",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Lime Gold",
                                            "subtitle": "Lime Gold Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia X Performance",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "Rose Gold Screen"
                                        },
                                        {
                                            "name": "Lime Gold",
                                            "subtitle": "Lime Gold Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia XA",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "Rose Gold Screen"
                                        },
                                        {
                                            "name": "Lime Gold",
                                            "subtitle": "Lime Gold Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia X",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "Rose Gold Screen"
                                        },
                                        {
                                            "name": "Lime Gold",
                                            "subtitle": "Lime Gold Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia Z5",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Gold",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Green",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia Z5 Compact",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia Z3+",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Copper",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Green",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia C4",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia M4 Aqua",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "Rose Gold Screen"
                                        },
                                        {
                                            "name": "Lime Gold",
                                            "subtitle": "Lime Gold Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia Z3",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Copper",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Green",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia Z3v",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia Z2",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Onyx",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Ceramic",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Xperia Z",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Onyx",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Ceramic",
                                            "subtitle": "White Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Huawei",
                        "models": [
                            {
                                "model": "Nexus 6P",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Aluminum",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Gold",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "P9",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Haze Gold",
                                            "subtitle": "Gold Screen"
                                        },
                                        {
                                            "name": "Ceramic White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Prestige Gold",
                                            "subtitle": "Gold Screen"
                                        },
                                        {
                                            "name": "Titanium Grey",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Mystic Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Red",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Other",
                        "models": [
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    }
                ]
            }
        },
        {
            "tablet": {
                "networks": [
                    "Wifi",
                    "Network"
                ],
                "issues": [
                    "Broken Screen",
                    "Battery",
                    "Liquid Damage",
                    "Won't Charge",
                    "Camera(s) Issue",
                    "Data Backup",
                    "Won't Turn On",
                    "Software Issue(s)",
                    "Audio Issue(s)",
                    "Button Issue(s)",
                    "Network Issue",
                    "Wifi/Bluetooth Issue",
                    "Other"
                ],
                "manufacturers": [
                    {
                        "name": "Apple",
                        "models": [
                            {
                                "model": "iPad Pro 9.7\"",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Rose Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad Pro 12.9\"",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad Air 2",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad Mini 4",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad Mini 3",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad Mini 2",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad Mini",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Slate",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        },
                                        {}
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad 4",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad 3",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad 2",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPad",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Samsung",
                        "models": [
                            {
                                "model": "Galaxy TabPro S",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy Tab S2",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy View",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Galaxy Tab",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "LG",
                        "models": [
                            {
                                "model": "G Pad X",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "G Pad F",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "G Pad 2",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "G Pad",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "HTC",
                        "models": [
                            {
                                "model": "Nexus",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "ASUS",
                        "models": [
                            {
                                "model": "Nexus",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "ZenPad",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Transformer Pad",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Vivo Tab",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "MeMO Tab",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Verizon",
                        "models": [
                            {
                                "model": "Ellipsis",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "GizmoTab",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "HP",
                        "models": [
                            {
                                "model": "Pro x2",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Elite x2",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Pro",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Dell",
                        "models": [
                            {
                                "model": "New Venue",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Venue",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Microsoft",
                        "models": [
                            {
                                "model": "Surface",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Amazon",
                        "models": [
                            {
                                "model": "Kindle",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    },
                    {
                        "name": "Other",
                        "models": [
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            }
                        ]
                    }
                ]
            }
        },
        {
            "computer": {
                "issues": [
                    "Liquid Damage",
                    "Won't Charge",
                    "Hard Drive Issue(s)",
                    "Virus/Malware",
                    "Data Backup",
                    "Won't Turn On",
                    "Software Issue(s)",
                    "Audio Issue(s)",
                    "Button Issue(s)",
                    "Wifi/Bluetooth Issue",
                    "Other"
                ],
                "manufacturers": [
                    {
                        "name": "Apple",
                        "models": [
                            {
                                "model": "Macbook Pro Retina 13\" (A1708)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Late 2016",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Pro Retina 13\" (A1706)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Late 2016",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
										"Touch Bar Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Pro Retina 13\" (A1502)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Early 2015",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2014",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2013",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Pro Retina 13\" (A1425)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Early 2013",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2012",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Pro Retina 15\" (A1707)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Late 2016",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
										"Touch Bar Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Pro Retina 15\" (A1398)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Mid 2015",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2014",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2013",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2013",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2012",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Pro Unibody 13\" (A1278)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Mid 2012",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2010",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2009",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Pro Unibody 15\" (A1286)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Mid 2012",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2010",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2009 (2.53 GHz)",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2009",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2008 / Early 2009",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Pro Unibody 17\" (A1297)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Late 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2010",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2009",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2009",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Pro 15\" (A1260)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Early 2008",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Air 13\" (A1466)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Early 2015",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2014",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2013",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2012",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Air 13\" (A1369)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Mid 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2010",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Air 13\" (A1304)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Mid 2009",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2008",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Air 13\" (A1237)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Early 2008",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Air 11\" (A1465)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Early 2015",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2014",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2013",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2012",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Air 11\" (A1370)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Mid 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2010",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Retina 12\" (A1534)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Early 2016",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2015",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Unibody 13\" (A1278)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Late 2008",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook Unibody 13\" (A1342)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Mid 2010",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2009",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Macbook 13\" (A1181)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Mid 2009",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2009",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2008",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2007",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2007",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2006",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2006",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Mac Mini Unibody (A1347)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Late 2014",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2012",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2010",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Battery"
                                    ]
                                }
                            },
                            {
                                "model": "iMac 21.5\" (A1418)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Late 2015",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2014",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2013",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Early 2013",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2012",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "iMac 21.5\" (A1311)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Late 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2010",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2009",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "iMac 27\" (A1419)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Late 2015",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2015",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2014",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2013",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2012",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "iMac 27\" (A1312)",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Mid 2011",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Mid 2010",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2009 (EMC 2374)",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Late 2009 (EMC 2309)",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "year": [
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Acer",
                        "models": [
                            {
                                "model": "Aspire",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Aspire One",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Extensa",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Ferrari",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "One",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Timeline",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "TravelMate",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "ASUS",
                        "models": [
                            {
                                "model": "ZenBook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Republic Of Gamers",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Dell",
                        "models": [
                            {
                                "model": "Latitude",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Inspiron",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Precision",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "XPS",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Alienware",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "HP",
                        "models": [
                            {
                                "model": "Pavilion",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "EliteBook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "OMEN",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "ProBook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Spectre",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Envy",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Stream",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "ZBook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Lenovo",
                        "models": [
                            {
                                "model": "Ideapad",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Thinkpad",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Yoga",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Flex",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Toshiba",
                        "models": [
                            {
                                "model": "Satellite",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Tecra",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Portege",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Sony",
                        "models": [
                            {
                                "model": "Vaio",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Samsung",
                        "models": [
                            {
                                "model": "Notebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Ativ Book",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "LG",
                        "models": [
                            {
                                "model": "Gram",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebase",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "MSI",
                        "models": [
                            {
                                "model": "Gaming Series",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Prestige Series",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Classic Series",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Google",
                        "models": [
                            {
                                "model": "Chromebook Pixel",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Microsoft",
                        "models": [
                            {
                                "model": "Surface Book",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Surface Studio",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Network Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Sager",
                        "models": [
                            {
                                "model": "Notebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Field Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Other",
                        "models": [
                            {
                                "model": "Laptop",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Desktop",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Chromebook",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            },
                            {
                                "model": "Other",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen",
                                        "Battery",
                                        "Keyboard Issue",
                                        "Trackpad Issue",
                                        "Webcam Issue",
                                        "Hinge/Body Issue"
                                    ]
                                }
                            }
                        ]
                    }
                ]
            }
        },
        {
            "other": {
                "issues": [
                    "Battery",
                    "Liquid Damage",
                    "Won't Charge",
                    "Camera(s) Issue",
                    "Data Backup",
                    "Won't Turn On",
                    "Software Issue(s)",
                    "Audio Issue(s)",
                    "Button Issue(s)",
                    "Wifi/Bluetooth Issue",
                    "Other"
                ],
                "manufacturers": [
                    {
                        "name": "Apple",
                        "models": [
                            {
                                "model": "iPod Touch",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Pink",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Red",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen"
                                    ]
                                }
                            },
                            {
                                "model": "iPod Nano",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "Silver",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Gold",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": "Black Screen"
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Pink",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Red",
                                            "subtitle": "White Screen"
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen"
                                    ]
                                }
                            },
                            {
                                "model": "iPod Shuffle",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "Silver",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Gold",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Space Gray",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Blue",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Pink",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Red",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": []
                                }
                            },
                            {
                                "model": "iPod Classic",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen"
                                    ]
                                }
                            },
                            {
                                "model": "iPod Mini",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen"
                                    ]
                                }
                            },
                            {
                                "model": "Apple Watch",
                                "info": {
                                    "model #": [
                                        {
                                            "name": "Text Box Please",
                                            "subtitle": ""
                                        }
                                    ],
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen"
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "name": "Other",
                        "models": [
                            {
                                "model": "Other",
                                "info": {
                                    "colors": [
                                        {
                                            "name": "White",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Black",
                                            "subtitle": ""
                                        },
                                        {
                                            "name": "Other",
                                            "subtitle": ""
                                        }
                                    ],
                                    "issue": [
                                        "Broken Screen"
                                    ]
                                }
                            }
                        ]
                    }
                ]
            }
        }
    ]
};
