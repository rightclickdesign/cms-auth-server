/* Campus Mobile Solutions - RepairShopr Authentication Server
 */
"use strict";
var express = require("express");
var bodyParser = require("body-parser");
var request = require("request");
var PORT = 9000;
var SECRET = process.env.REPAIRSHOPR_API_KEY;
var SENDGRID_API_KEY = process.env.SENDGRID_API_KEY;
var SENDGRID_TEMPLATE_ID = '4b0dc3e2-c23a-4bd8-bebd-3ba879c397a9';
var sendgrid = require('sendgrid');
var app = express();
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'https://www.campusmobilesolutions.com');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
var checkInSummaryEmail = function (opts) {
    console.log('send email started');
    var helper = sendgrid.mail;
    var fromEmail = new helper.Email('cms@campusmobilesolutions.com', 'Campus Mobile Solutions');
    var toEmail = new helper.Email(opts.customer_email);
    var subject = opts.manufacturer + " " + opts.model + " Repair - Ticket #" + opts.ticketNumber + " (message id: " + opts.ticketId + ")";
    var content = new helper.Content('text/html', ' ');
    var mail = new helper.Mail(fromEmail, subject, toEmail, content);
    var category = new helper.Category("check-in-summary");
    var manufacturerField = '';
    var modelField = '';
    var colorField = '';
    var networkField = '';
    var imeiField = '';
    var preexistingConditionsField = '';
    var additionalDetailsField = '';
    var issuesField = '';
    var screenQualityField = '';
    // Device Information Block
    if (opts.manufacturer) {
        manufacturerField = "Manufacturer: " + opts.manufacturer + "<br/>";
    }
    if (opts.model) {
        modelField = "Model: " + opts.model + "<br/>";
    }
    if (opts.color) {
        colorField = "Color: " + opts.color + "<br/>";
    }
    if (opts.network) {
        networkField = "Carrier: " + opts.network + "<br/>";
    }
    if (opts.imei) {
        imeiField = "Serial / IMEI: " + opts.imei + "<br/>";
    }
    if (opts.preexistingConiditions) {
        preexistingConditionsField = "Pre-existing Conditions: " + opts.preexistingConiditions + "<br/>";
    }
    if (opts.additionalDetails) {
        additionalDetailsField = "Additional Details: " + opts.additionalDetails + "<br/>";
    }
    var deviceInformationBlock = "\n            " + manufacturerField + " \n            " + modelField + " \n            " + colorField + "\n            " + networkField + "\n            " + imeiField + "\n            " + preexistingConditionsField + "\n            " + additionalDetailsField + "\n        ";
    // Repair Information Block
    if (opts.issue) {
        issuesField = "Issue: " + opts.issue + "<br/>";
    }
    if (opts.screenQuality) {
        screenQualityField = "Selected Screen Quality: " + opts.screenQuality + "<br/>";
    }
    var repairInformationBlock = "\n            " + issuesField + "\n            " + screenQualityField + "\n        ";
    mail.addCategory(category);
    // Fill in Email Variables
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{device_information}}', deviceInformationBlock));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{repair_information}}', repairInformationBlock));
    // Customer Information block
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{customer_name}}', opts.customer_name));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{phone}}', opts.customer_phone));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{email}}', opts.customer_email));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{ticket_number}}', opts.ticketNumber));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{comment_subject}}', opts.comment_subject));
    // Device and Repair information is now sent as blocks (see deviceInformationBlock and repairInformationBlock)
    // Individual substitutions can be used as variables in email template
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{manufacturer}}', opts.manufacturer));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{model}}', opts.model));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{year}}', opts.year));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{color}}', opts.color));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{network}}', opts.network));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{imei}}', opts.imei));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{preexisting_conditions}}', opts.preexistingConiditions));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{additional_details}}', opts.additionalDetails));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{issue}}', opts.issue));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{screen_quality}}', opts.screenQuality));
    mail.setTemplateId(SENDGRID_TEMPLATE_ID);
    return mail.toJSON();
};
var sendEmail = function (toSend) {
    console.log(JSON.stringify(toSend, null, 2));
    //console.log(JSON.stringify(toSend))
    var sg = require('sendgrid')(SENDGRID_API_KEY);
    var requestBody = toSend;
    var emptyRequest = require('sendgrid-rest').request;
    var requestPost = JSON.parse(JSON.stringify(emptyRequest));
    requestPost.method = 'POST';
    requestPost.path = '/v3/mail/send';
    requestPost.body = requestBody;
    sg.API(requestPost, function (error, response) {
        console.log(response.statusCode);
        console.log(response.body);
        console.log(response.headers);
    });
};
var createAsset = function (customerId, submitForm, forceAssetCreation, callback, dupAssetCallback) {
    console.log('app: begin createAsset()');
    var assetPayload;
    var fields = {};
    // Generically assign field variables
    for (var _i = 0, _a = submitForm.fields; _i < _a.length; _i++) {
        var field = _a[_i];
        fields[field.internalName] = field.field;
    }
    console.log('app: createAsset fields =\n' + JSON.stringify(fields, null, 4));
    console.log('app: createAsset type = ' + submitForm.deviceType);
    if (submitForm.deviceType === 'Phone') {
        var phonePayload = {
            "asset_type_id": "33572",
            "asset_serial": submitForm.serialOrImei,
            "name": fields.manufacturer + ' ' + fields.model,
            "customer_id": customerId,
            "properties": {
                "Manufacturer": fields.manufacturer,
                "Model": fields.model,
                "Color": fields.color,
                "Network": fields.network
            }
        };
        assetPayload = phonePayload;
    }
    else if (submitForm.deviceType === 'Tablet') {
        var tabletPayload = {
            "asset_type_id": "33573",
            "asset_serial": submitForm.serialOrImei,
            "name": fields.manufacturer + ' ' + fields.model,
            "customer_id": customerId,
            "properties": {
                "Manufacturer": fields.manufacturer,
                "Model": fields.manufacturer + ' ' + fields.model,
                "Model Number": fields.specificModel,
                "Year": null,
                "Color": fields.color,
                "Network": fields.network
            }
        };
        assetPayload = tabletPayload;
    }
    else if (submitForm.deviceType === 'Computer') {
        var computerPayload = {
            "asset_type_id": "18387",
            "asset_serial": submitForm.serialOrImei,
            "name": fields.manufacturer + ' ' + fields.model,
            "customer_id": customerId,
            "properties": {
                "Manufacturer": fields.manufacturer,
                "Model": fields.model,
                "Model Number": fields.specificModel,
                "Year": fields.year,
                "Service Tag": null
            }
        };
        assetPayload = computerPayload;
    }
    else {
        var otherPayload = {
            "asset_type_id": "33931",
            "asset_serial": submitForm.serialOrImei,
            "name": fields.manufacturer + ' ' + fields.model,
            "customer_id": customerId,
            "properties": {
                "Manufacturer": fields.manufacturer,
                "Model": fields.model,
                "Model Number": fields.specificModel,
                "Color": fields.color
            }
        };
        assetPayload = otherPayload;
    }
    console.log('app: assetPayload =\n' + JSON.stringify(assetPayload, null, 4));
    var requestOptions = {
        method: 'POST',
        url: "https://cmsuiuc.repairshopr.com/api/v1/customer_assets?api_key=" + SECRET,
        json: assetPayload
    };
    console.log('app: create asset');
    console.log('repairshopr: post to /api/v1/customer_assets');
    console.log('repairshopr: requestOptions =\n' + JSON.stringify(requestOptions, null, 4));
    request(requestOptions, function (err, httpResponse, body) {
        console.log('repairshopr: createAsset post response =\n' + JSON.stringify(body, null, 4));
        if (body.success === false && body.message == 'Asset serial has already been taken') {
            var requestOptions_1 = {
                method: 'GET',
                url: "https://cmsuiuc.repairshopr.com/api/v1/customer_assets?api_key=" + SECRET
            };
            if (forceAssetCreation) {
                console.log('app: since asset exists, get asset id');
                request(requestOptions_1, function (err, httpResponse, body) {
                    var assets = JSON.parse(body).assets;
                    var assetSerial = submitForm.serialOrImei;
                    console.log('repairshopr: get response: assets list');
                    var foundAsset = null;
                    for (var _i = 0, assets_1 = assets; _i < assets_1.length; _i++) {
                        var asset = assets_1[_i];
                        if (asset.asset_serial === assetSerial) {
                            foundAsset = asset;
                            break;
                        }
                    }
                    console.log('repairshopr: asset found =\n' + JSON.stringify(foundAsset, null, 4));
                    callback(err, foundAsset);
                });
            }
            else {
                dupAssetCallback();
            }
        }
        else {
            console.log('repairshopr: sending body.asset =\n' + JSON.stringify(body.asset, null, 4));
            callback(err, body.asset);
        }
    });
};
var createTicket = function (assetId, customerId, customer, submitForm, callback) {
    console.log('app: begin createTicket()');
    var ticketTypeId = '';
    var ticketSubject = '';
    var ticketCommentSubject = '';
    var fields = {};
    // Generically assign field variables
    for (var _i = 0, _a = submitForm.fields; _i < _a.length; _i++) {
        var field = _a[_i];
        fields[field.internalName] = field.field;
    }
    if (submitForm.deviceType === 'Computer') {
        ticketTypeId = '16879';
    }
    else {
        ticketTypeId = '10508';
    }
    console.log('app: createTicket fields =\n' + JSON.stringify(fields, null, 4));
    if (fields.color) {
        ticketSubject = fields.color + ' ' + fields.manufacturer + ' ' + fields.model + ' - ' + fields.issue;
    }
    else {
        ticketSubject = fields.manufacturer + ' ' + fields.model + ' - ' + fields.issue;
    }
    var ticket = {
        "subject": "" + ticketSubject,
        "customer_id": customerId,
        "comment_subject": "Initial Issue",
        "comment_body": "\n            " + fields.issue + " ",
        "comment_do_not_email": '1',
        "problem_type": fields.issue,
        "properties": {
            "Tested Before By": submitForm.technician,
            "Additional Details": submitForm.additionalDetails,
            "Pre-existing Damage": submitForm.additionalDamage,
            "Part Quality": submitForm.iPhoneQuality,
            "Password": submitForm.password,
            "Visual Inspection": submitForm.visualInspection
        },
        "ticket_type_id": ticketTypeId,
        "asset_ids": assetId
    };
    var requestOptions = {
        method: 'POST',
        url: "https://cmsuiuc.repairshopr.com/api/v1/tickets?api_key=" + SECRET,
        json: ticket
    };
    console.log('app: create ticket');
    console.log('repairshopr: post to /api/v1/tickets');
    console.log('repairshopr: requestOptions =\n' + JSON.stringify(requestOptions, null, 4));
    request(requestOptions, function (err, httpResponse, body) {
        console.log('repairshopr: createTicket ticket.id = ' + body.ticket.id);
        var mailOpts = {
            comment_subject: ticketCommentSubject,
            customer_name: customer.firstname + " " + customer.lastname,
            customer_email: customer.email,
            customer_phone: customer.mobile,
            deviceType: fields.deviceType,
            manufacturer: fields.manufacturer,
            model: fields.model,
            year: fields.year,
            color: fields.color,
            network: fields.network,
            issue: fields.issue,
            imei: submitForm.serialOrImei,
            preexistingConiditions: submitForm.additionalDamage,
            additionalDetails: submitForm.additionalDetails,
            screenQuality: submitForm.iPhoneQuality,
            ticketId: body.ticket.id.toString(),
            ticketNumber: body.ticket.number.toString()
        };
        sendEmail(checkInSummaryEmail(mailOpts));
        callback(err);
    });
};
app.post('/api/check_in', function (req, res) {
    try {
        console.log('post call started');
        console.log('post: req.body.contact =\n' + JSON.stringify(req.body.contact, null, 4));
        console.log('post: req.body.form =\n' + JSON.stringify(req.body.form, null, 4));
        var body = req.body;
        var contactInfo = req.body.contact;
        var submitForm_1 = req.body.form;
        submitForm_1.password = contactInfo.password;
        var email_1 = contactInfo.email.toLowerCase().trim();
        var forceAssetCreation_1 = submitForm_1.forceAssetCreation;
        var customer_1 = {
            firstname: contactInfo.firstName,
            lastname: contactInfo.lastName,
            email: email_1,
            mobile: contactInfo.phone,
            business_name: '',
            address: '',
            address_2: '',
            city: '',
            state: '',
            zip: '',
            properties: {
                'Lead Source': contactInfo.leadSource
            }
        };
        var requestOptions_2 = {
            method: 'POST',
            url: "https://cmsuiuc.repairshopr.com/api/v1/customers?api_key=" + SECRET,
            json: customer_1
        };
        request(requestOptions_2, function (err, httpResponse, body) {
            console.log('repairshopr: post to /api/v1/customers');
            console.log('repairshopr: requestOptions =\n' + JSON.stringify(requestOptions_2, null, 4));
            if (err) {
                res.status(400).send('There was an error creating the customer.');
            }
            else {
                if (body.success === false) {
                    console.log('repairshopr: post to /api/v1/customers response: body.success === ' + body.success);
                    console.log('repairshopr: body.message = ' + body.message);
                    var requestOptions_3 = {
                        method: 'GET',
                        url: "https://cmsuiuc.repairshopr.com/api/v1/customers?api_key=" + SECRET
                    };
                    console.log('app: since customer exists, get customer id');
                    console.log('repairshopr: get from /api/v1/customers');
                    console.log('repairshopr: requestOptions =\n' + JSON.stringify(requestOptions_3, null, 4));
                    request(requestOptions_3, function (err, httpResponse, body) {
                        if (err) {
                            res.status(400).send('There was an error retrieving customers.');
                        }
                        else {
                            var customers = JSON.parse(body).customers;
                            console.log('repairshopr: get response: customers list');
                            var foundCustomer = null;
                            for (var _i = 0, customers_1 = customers; _i < customers_1.length; _i++) {
                                var customer_2 = customers_1[_i];
                                console.log('repairshopr: customer.email = ' + customer_2.email);
                                console.log('repairshopr: customer.id = ' + customer_2.id);
                                if (customer_2.email === email_1) {
                                    foundCustomer = customer_2;
                                    console.log('repairshopr: foundCustomer.email/id = ' + foundCustomer.email + '/' + foundCustomer.id);
                                    break;
                                }
                            }
                            if (!foundCustomer) {
                                console.log('repairshopr: customer email exists in repairshopr but could not be retrieved');
                                res.status(400).send('CREATE_CUSTOMER_ERROR');
                            }
                            else {
                                console.log('repairshopr: customer found =\n' + JSON.stringify(foundCustomer, null, 4));
                                var customerId_1 = foundCustomer.id;
                                console.log('repairshopr: customerId: ' + customerId_1);
                                createAsset(customerId_1, submitForm_1, forceAssetCreation_1, function (err, body) {
                                    // Success
                                    if (err) {
                                        res.status(400).send('There was an error creating the asset. Customer existed.');
                                    }
                                    else {
                                        console.log('app: complete createAsset()');
                                        var assetId = body.id;
                                        createTicket(assetId, customerId_1, customer_1, submitForm_1, function (err) {
                                            if (err) {
                                                res.status(400).send('There was an error creating the asset. Customer existed.');
                                            }
                                            else {
                                                console.log('app: complete createTicket()');
                                                res.status(200).send('Success');
                                            }
                                        });
                                    }
                                }, function () {
                                    // Duplicate asset
                                    res.status(400).send('DUP_ASSET');
                                });
                            }
                        }
                    });
                }
                else {
                    console.log('repairshopr: post to /api/v1/customers response: customer did not exist, created');
                    console.log('repairshopr: new customer =\n' + JSON.stringify(body.customer, null, 4));
                    var customerId_2 = body.customer.id;
                    createAsset(customerId_2, submitForm_1, forceAssetCreation_1, function (err, body) {
                        // Success
                        if (err) {
                            res.status(400).send('There was an error creating the asset. Customer was created.');
                        }
                        else {
                            var assetId = body.id;
                            createTicket(assetId, customerId_2, customer_1, submitForm_1, function (err) {
                                if (err) {
                                    res.status(400).send('There was an error creating the asset. Customer was created.');
                                }
                                else {
                                    console.log('res.status.send() from createTicket');
                                    res.status(200).send('Success');
                                }
                            });
                        }
                    }, function () {
                        // Duplicate asset
                        res.status(400).send('DUP_ASSET');
                    });
                }
            }
        });
    }
    catch (e) {
        res.status(400).send('There was an unknown error: ' + e);
    }
});
app.get('/api/check_in', function (req, res) {
    res.json({
        chance: req.chance
    });
    console.log('get: ' + req.url);
});
app.get('/send-test-email-tommy-private', function (req, res) {
    console.log('sending email');
    var mailOpts = {
        comment_subject: 'We have your device',
        customer_name: "Tommy Nguyen",
        customer_email: 'tommy@rcd.cool',
        customer_phone: '1234567890',
        deviceType: 'Laptop',
        manufacturer: 'Apple',
        model: 'MacBook Pro',
        year: '2015',
        color: 'White',
        network: 'Sprint',
        issue: 'Broken Screen',
        imei: Math.floor(Math.random() * 1000000).toString(),
        preexistingConiditions: '',
        additionalDetails: 'Left charger with us',
        screenQuality: '',
        ticketId: 'id' + Math.floor(Math.random() * 10000)
    };
    sendEmail(checkInSummaryEmail(mailOpts));
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send('<html><body><p>Test email sent with the following info</p>' + '<pre>' + JSON.stringify(mailOpts, null, 2) + '</pre></body></html>');
    console.log('200: ' + req.url);
});
app.get('/send-test-email-miko-private', function (req, res) {
    console.log('sending email');
    var mailOpts = {
        comment_subject: 'We have your device',
        customer_name: "Miko",
        customer_email: 'aamikam@gmail.com',
        customer_phone: '1234567890',
        deviceType: 'Laptop',
        manufacturer: 'Apple',
        model: 'MacBook Pro',
        year: '2015',
        color: null,
        network: null,
        issue: 'Broken Screen',
        imei: Math.floor(Math.random() * 1000000).toString(),
        preexistingConiditions: 'Scratched screen',
        additionalDetails: 'Left charger with us',
        screenQuality: 'OEM',
        ticketId: 'id' + Math.floor(Math.random() * 100000)
    };
    sendEmail(checkInSummaryEmail(mailOpts));
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send('<html><body><p>Test email sent with the following info</p>' + '<pre>' + JSON.stringify(mailOpts, null, 2) + '</pre></body></html>');
    console.log('200: ' + req.url);
});
app.get('*', function (req, res) {
    res.status(404).send('You are at the wrong place2.');
    console.log('404: ' + req.url);
});
app.listen(PORT, function (err) {
    if (err) {
        return console.log('server error: ', err);
    }
    console.log('Server listening on: http://localhost:%s', PORT);
});
