declare const angular;
declare const REPAIR_TYPES;

// nginx -> node proxy
const API_ENDPOINT = 'https://cms.rcd.cool/api/check_in';
//const API_ENDPOINT = 'http://localhost:9000/api';

class Util {
    static NoFieldUrlKey = 'n';
    static SpecificModelSearchQuery = 'specificModel';
    static SpecificYearSearchQuery = 'specificYear';
    static SpecificColorSearchQuery = 'specificColor';
    static SpecificManufacturerSearchQuery = 'specificManufacturer';

    static GetUrlFriendlyKey(str: string) {
        str = str.replace(new RegExp(' ', 'g'), '-');
        str = str.replace(new RegExp('/', 'g'), '_');
        return str;
    }

    static GetKeyFromUrlFriendly(str: string) {
        let key = str.replace(new RegExp('-', 'g'), ' ');
        key = key.replace(new RegExp('_', 'g'), '/');
        return key;
    }

    static GetDeviceType(deviceType: string) {
        for (let type of REPAIR_TYPES.repairTypes) {
            for (let key in type) {
                if (key === deviceType) {
                    return type[key];
                }
            }
        }
    }

    static FriendlyNameForDeviceType(deviceType: string) {
        switch (deviceType) {
            case 'phone':
                return 'Phone';
            case 'tablet':
                return 'Tablet';
            case 'computer':
                return 'Computer';
            case 'other':
                return 'Other';
        }
    }

    static GetManufacturersForDeviceType(deviceType: string) {
        return Util.GetDeviceType(deviceType).manufacturers;
    }

    static GetModels(deviceType: string, manufacturerName: string) {
        let manufacturers = Util.GetManufacturersForDeviceType(deviceType);
        for (let manufacturer of manufacturers) {
            if (manufacturer.name === manufacturerName) {
                return manufacturer.models;
            }
        }
    }

    static GetModel(deviceType: string, manufacturerName: string, modelName: string) {
        let manufacturers = Util.GetManufacturersForDeviceType(deviceType);
        let models = Util.GetModels(deviceType, manufacturerName);
        for (let model of models) {
            if (model.model === modelName) {
                return model;
            }
        }
    }

    static NavigateForward($location, key: string, numToSkip?: number) {
        let pathKey = Util.GetUrlFriendlyKey(key);
        let path = $location.path() + `/${pathKey}`;
        if (numToSkip) {
            for (let i = 0; i < numToSkip; i++) {
                path += `/${Util.NoFieldUrlKey}`;
            }
        }
        $location.path(path);
    }

    static NavigateBack($location) {
        let path = $location.path().substring(0, $location.path().lastIndexOf('/'));
        $location.path(path);
    }

    static NavigateHome($location) {
            $location.path('');
    }
}

angular.module('CMSApp', ['ngRoute'])
    .config(function ($routeProvider, $locationProvider) {

        $routeProvider
            .when('/', {
                templateUrl: 'hello.html',
                controller: 'HelloController'
            })
            .when('/device', {
                templateUrl: 'device-start.html',
                controller: 'DeviceController'
            })
            .when('/device/:deviceType', {
                templateUrl: 'manufacturers.html',
                controller: 'ManufacturerController'
            })
            .when('/device/:deviceType/:manufacturer', {
                templateUrl: 'models.html',
                controller: 'ModelsController'
            })
            .when('/device/:deviceType/:manufacturer/:model', {
                templateUrl: 'years.html',
                controller: 'YearsController'
            })
            .when('/device/:deviceType/:manufacturer/:model/:year', {
                templateUrl: 'colors.html',
                controller: 'ColorsController'
            })
            .when('/device/:deviceType/:manufacturer/:model/:year/:color', {
                templateUrl: 'networks.html',
                controller: 'NetworkController'
            })
            .when('/device/:deviceType/:manufacturer/:model/:year/:color/:network', {
                templateUrl: 'issues.html',
                controller: 'IssuesController'
            })
            .when('/device/:deviceType/:manufacturer/:model/:year/:color/:network/:issue', {
                templateUrl: 'contact.html',
                controller: 'ContactController'
            })
            .when('/device/:deviceType/:manufacturer/:model/:year/:color/:network/:issue/:submit', {
                templateUrl: 'submit.html',
                controller: 'SubmitController'
            });

        // use the HTML5 History API
        $locationProvider.html5Mode(false).hashPrefix('');
    })

    .service('stateService', function () {
        this.contactInfo = null;
        this.isComingFromSuccess = false;
        this.breadcrumbBackArrowHref = '#/';
        this.breadcrumbs = [];
    })

    .controller('MainController', function ($scope, $location, stateService) {
        $scope.$on('$locationChangeStart', function (event) {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
        $scope.stateService = stateService;
        $scope.NoFieldUrlKey = Util.NoFieldUrlKey;
    })

    .controller('HelloController', function ($scope, $location, stateService) {

        stateService.contactInfo = null;
        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            }
        ];
        stateService.breadcrumbBackArrowHref = '#/';

        if (stateService.isComingFromSuccess) {
            $scope.showSuccess = true;
            setTimeout(() => {
                $scope.showSuccess = false;
                $scope.$apply();
            })
        }

        $scope.startClicked = () => {
            $location.path('/device');
        }

    })

    .controller('DeviceController', function ($scope, $location, stateService) {

        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            },
            {
                subtitle: 'Device',
                title: 'Choosing',
                href: '#/device'
            }
        ]
        stateService.breadcrumbBackArrowHref = '#/';

        $scope.showSpecificModelPrompt = false;
        $scope.showSpecificModelError = false;
        let selectedChoice;

        $scope.choices = ['Phone', 'Tablet', 'Computer', 'Other'];
        $scope.choiceClicked = (choice) => {
            let choicePath = choice.toLowerCase();
            $location.path(`/device/${choicePath}`)
        }

    })

    .controller('ManufacturerController', function ($scope, $location, stateService, $route) {

        let deviceType = $route.current.params.deviceType;

        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            },
            {
                subtitle: 'Device',
                title: Util.FriendlyNameForDeviceType(deviceType),
                href: `#/device/`
            },
            {
                subtitle: 'Manufacturer',
                title: 'Choosing',
                href: `#/device/${deviceType}`
            }
        ]
        stateService.breadcrumbBackArrowHref = '#/device/';

        let manufacturers = Util.GetManufacturersForDeviceType(deviceType);
        for (let i = 0; i < manufacturers.length; i++) {
            if (!manufacturers[i].name) {
                manufacturers.splice(i, 1);
            }
        }

        $scope.deviceTypeName = Util.FriendlyNameForDeviceType(deviceType) !== 'Other' ? Util.FriendlyNameForDeviceType(deviceType) : '';
        $scope.manufacturers = manufacturers;

        $scope.showSpecificModelPrompt = false;
        $scope.showSpecificModelError = false;
        let selectedManufacturer;

        $scope.manufacturerClicked = (manufacturer) => {
            //console.log(manufacturer);
            if (manufacturer.name === 'Other') {
                selectedManufacturer = manufacturer;
                $scope.showSpecificModelPrompt = true;
                setTimeout(() => {
                    document.getElementById('specificModelInput').focus();
                }, 20);
            } else {
                Util.NavigateForward($location, manufacturer.name);
            }
        }

        $scope.nextClicked = () => {
            if ($scope.specificModel) {
                $location.search(Util.SpecificManufacturerSearchQuery, $scope.specificModel);
                Util.NavigateForward($location, selectedManufacturer.name);
            } else {
                $scope.showSpecificModelError = true;
            }
        }

    })

    .controller('ModelsController', function ($scope, $location, stateService, $route) {

        let deviceTypeName = $route.current.params.deviceType;
        let manufacturer = $route.current.params.manufacturer;
        let friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturer);
        let models = Util.GetModels(deviceTypeName, friendlyManufacturer);

        for (let model of models) {
            model.activated = false;
        }

        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            },
            {
                subtitle: 'Device',
                title: Util.FriendlyNameForDeviceType(deviceTypeName),
                href: `#/device/`
            },
            {
                subtitle: 'Manufacturer',
                title: friendlyManufacturer,
                href: `#/device/${deviceTypeName}/`
            },
            {
                subtitle: 'Model',
                title: 'Choosing',
                href: `#/device/${deviceTypeName}/${manufacturer}`
            }
        ]
        stateService.breadcrumbBackArrowHref = `#/device/${deviceTypeName}/`;

        $scope.selectedModel = null;
        $scope.models = models;
        $scope.showSpecificModelPrompt = false;
        $scope.specificModel = '';

        $scope.deviceTypeName = Util.FriendlyNameForDeviceType(deviceTypeName) !== 'Other' ? Util.FriendlyNameForDeviceType(deviceTypeName) : '';
        $scope.manufacturerName = friendlyManufacturer !== 'Other' ? friendlyManufacturer : '';

        let doNavigate = () => {
            // Check to see if we do have year and / or colors and / or network.
            // If no year or color or network, jump to Issues
            // If no year or color, jump to Network.
            // If no year but color, jump to Color.
            // Else go forward

            let deviceType = Util.GetDeviceType(deviceTypeName);
            let hasYear = $scope.selectedModel.info && $scope.selectedModel.info.year;
            let hasColors = $scope.selectedModel.info && $scope.selectedModel.info.colors;
            let hasNetwork = deviceType.networks;

            if (!hasYear && !hasColors && !hasNetwork) {
                Util.NavigateForward($location, $scope.selectedModel.model, 3);
            } else if (!hasYear && !hasColors) {
                Util.NavigateForward($location, $scope.selectedModel.model, 2);
            } else if (!hasYear) {
                Util.NavigateForward($location, $scope.selectedModel.model, 1);
            } else {
                Util.NavigateForward($location, $scope.selectedModel.model);
            }
        }

        $scope.modelClicked = (model) => {
            // OLD: If Apple, list all Mac models. If not Apple, list general models then prompt for specific model with text field
            $scope.selectedModel = model;

            for (let model of $scope.models) {
                model.activated = false;
            }
            model.activated = true;

            // if (deviceTypeName === 'computer') {
            //     let isApple = friendlyManufacturer === 'Apple';
            //     if (!isApple) {
            //         $scope.showSpecificModelPrompt = true;
            //         setTimeout(() => {
            //             document.getElementById('specificModelInput').focus();
            //         }, 20);
            //     } else {
            //         doNavigate();
            //     }
            // } else
            if (model.model === 'Other' || (model.info && model.info['model #'])) {
                $scope.showSpecificModelPrompt = true;
                setTimeout(() => {
                    document.getElementById('specificModelInput').focus();
                }, 20);
            } else {
                doNavigate();
            }
        }

        $scope.nextClicked = () => {
            if ($scope.specificModel) {
                $location.search(Util.SpecificModelSearchQuery, $scope.specificModel);
                doNavigate();
            } else {
                $scope.showSpecificModelError = true;
            }
        }
    })

    .controller('YearsController', function ($scope, $location, stateService, $route) {

        let deviceTypeName = $route.current.params.deviceType;
        let manufacturerName = $route.current.params.manufacturer;
        let friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturerName);
        let friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
        let model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);

        // Check if years exists first. if not, navigate back one more time
        let hasYear = model.info && model.info.year;

        if (!hasYear) {
            Util.NavigateBack($location);
        }

        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            },
            {
                subtitle: 'Device',
                title: Util.FriendlyNameForDeviceType(deviceTypeName),
                href: `#/device/`
            },
            {
                subtitle: 'Manufacturer',
                title: friendlyManufacturer,
                href: `#/device/${deviceTypeName}/`
            },
            {
                subtitle: 'Model',
                title: model.model,
                href: `#/device/${deviceTypeName}/${manufacturerName}`
            },
            {
                subtitle: 'Year',
                title: 'Choosing',
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}`
            }
        ]
        stateService.breadcrumbBackArrowHref = `#/device/${deviceTypeName}/${manufacturerName}`;

        $scope.deviceTypeName = Util.FriendlyNameForDeviceType(deviceTypeName);
        $scope.years = model.info.year;

        $scope.showSpecificModelPrompt = false;
        $scope.showSpecificModelError = false;
        let selectedYear;

        let doNavigate = (theYear) => {
            // Check to see if we do have colors and / or network.
            // If no colors or network, jump to Issues
            // If no colors but network, jump to Network.
            // Else go forward
            let deviceType = Util.GetDeviceType(deviceTypeName);
            let hasColors = model.info && model.info.colors;
            let hasNetwork = deviceType.networks;
            if (!hasColors && !hasNetwork) {
                Util.NavigateForward($location, theYear.name, 2);
            } else if (!hasColors) {
                Util.NavigateForward($location, theYear.name, 1);
            } else {
                Util.NavigateForward($location, theYear.name);
            }
        }


        $scope.yearClicked = (year) => {
            if (year.name === 'Other') {
                selectedYear = year;
                $scope.showSpecificYearPrompt = true;
                setTimeout(() => {
                    document.getElementById('specificYearInput').focus();
                }, 20);
            } else {
                doNavigate(year);
            }
        }

        $scope.nextClicked = () => {
            if ($scope.specificYear) {
                $location.search(Util.SpecificYearSearchQuery, $scope.specificYear);
                doNavigate(selectedYear);
            } else {
                $scope.showSpecificYearError = true;
            }
        }

    })

    .controller('ColorsController', function ($scope, $location, stateService, $route) {

        let deviceTypeName = $route.current.params.deviceType;
        let manufacturerName = $route.current.params.manufacturer;
        let friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturerName);
        let friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
        let friendlyYear = Util.GetKeyFromUrlFriendly($route.current.params.year)
        let model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);

        // Check if years colors exists first. if not, navigate back one more time
        let hasColors = model.info && model.info.colors;

        if (!hasColors) {
            Util.NavigateBack($location);
        }

        stateService.comingFromField = 'color';
        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            },
            {
                subtitle: 'Device',
                title: Util.FriendlyNameForDeviceType(deviceTypeName),
                href: `#/device/`
            },
            {
                subtitle: 'Manufacturer',
                title: friendlyManufacturer,
                href: `#/device/${deviceTypeName}/`
            },
            {
                subtitle: 'Model',
                title: model.model,
                href: `#/device/${deviceTypeName}/${manufacturerName}`
            },
            {
                subtitle: 'Year',
                title: friendlyYear,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}`
            },
            {
                subtitle: 'Color',
                title: 'Choosing',
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}`
            }
        ]
        stateService.breadcrumbBackArrowHref = `#/device/${deviceTypeName}/${manufacturerName}/${model.model}`;

        $scope.colors = model.info.colors;

        $scope.showSpecificModelPrompt = false;
        $scope.showSpecificModelError = false;
        let selectedColor;

        let doNavigate = (theColor) => {
            // Check to see if we do have a network.
            // If no network, skip.
            let deviceType = Util.GetDeviceType(deviceTypeName);
            let hasNetwork = deviceType.networks;
            if (!hasNetwork) {
                Util.NavigateForward($location, theColor.name, 1);
            } else {
                Util.NavigateForward($location, theColor.name);
            }
        }


        $scope.colorClicked = (color) => {
            if (color.name === 'Other') {
                selectedColor = color;
                $scope.showSpecificModelPrompt = true;
                setTimeout(() => {
                    document.getElementById('specificModelInput').focus();
                }, 20);
            } else {
                doNavigate(color);
            }
        }

        $scope.nextClicked = () => {
            if ($scope.specificModel) {
                $location.search(Util.SpecificColorSearchQuery, $scope.specificModel);
                doNavigate(selectedColor);
            } else {
                $scope.showSpecificModelError = true;
            }
        }

    })

    .controller('NetworkController', function ($scope, $location, stateService, $route) {

        let deviceTypeName = $route.current.params.deviceType;
        let deviceType = Util.GetDeviceType(deviceTypeName);
        let manufacturerName = $route.current.params.manufacturer;
        let friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturerName);
        let friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
        let friendlyYear = Util.GetKeyFromUrlFriendly($route.current.params.year);
        let friendlyColor = Util.GetKeyFromUrlFriendly($route.current.params.color);
        let model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);

        let networks = deviceType.networks;

        // Check if years networks exists first. if not, navigate back one more time
        if (!networks) {
            Util.NavigateBack($location);
        }

        stateService.comingFromField = 'network';
        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            },
            {
                subtitle: 'Device',
                title: Util.FriendlyNameForDeviceType(deviceTypeName),
                href: `#/device/`
            },
            {
                subtitle: 'Manufacturer',
                title: friendlyManufacturer,
                href: `#/device/${deviceTypeName}/`
            },
            {
                subtitle: 'Model',
                title: model.model,
                href: `#/device/${deviceTypeName}/${manufacturerName}`
            },
            {
                subtitle: 'Year',
                title: friendlyYear,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}`
            },
            {
                subtitle: 'Color',
                title: friendlyColor,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}`
            },
            {
                subtitle: 'Network',
                title: 'Choosing',
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}/${friendlyColor}`
            }
        ]
        stateService.breadcrumbBackArrowHref = `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}`;

        $scope.networks = networks;

        $scope.networkClicked = (network) => {
            Util.NavigateForward($location, network);
        }
    })

    .controller('IssuesController', function ($scope, $location, stateService, $route) {

        let deviceTypeName = $route.current.params.deviceType;
        let deviceType = Util.GetDeviceType(deviceTypeName);
        let issues = deviceType.issues;
        let manufacturerName = $route.current.params.manufacturer;
        let friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturerName);
        let friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
        let friendlyYear = Util.GetKeyFromUrlFriendly($route.current.params.year);
        let color = $route.current.params.color;
        let friendlyColor = Util.GetKeyFromUrlFriendly(color);
        let network = $route.current.params.network;
        let model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);

        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            },
            {
                subtitle: 'Device',
                title: Util.FriendlyNameForDeviceType(deviceTypeName),
                href: `#/device/`
            },
            {
                subtitle: 'Manufacturer',
                title: friendlyManufacturer,
                href: `#/device/${deviceTypeName}/`
            },
            {
                subtitle: 'Model',
                title: model.model,
                href: `#/device/${deviceTypeName}/${manufacturerName}`
            },
            {
                subtitle: 'Year',
                title: friendlyYear,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}`
            },
            {
                subtitle: 'Color',
                title: friendlyColor,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}`
            },
            {
                subtitle: 'Network',
                title: network,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}/${color}`
            },
            {
                subtitle: 'Issue',
                title: 'Choosing',
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}/${color}/${network}`
            }
        ]

        stateService.breadcrumbBackArrowHref = `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}/${color}`;

        if (model.info && model.info.issue) {
            let issuesCombined = issues.slice(0);
            let index = issuesCombined.length;

            issuesCombined.splice.apply(issuesCombined, [index-1, 0].concat(model.info.issue));
            $scope.issues = issuesCombined;
        } else {
            $scope.issues = issues;
        }

        $scope.issueClicked = (issue) => {
            Util.NavigateForward($location, issue);
        }

    })

    .controller('ContactController', function ($scope, $location, stateService, $route) {

        let deviceTypeName = $route.current.params.deviceType;
        let friendlyManufacturer = Util.GetKeyFromUrlFriendly($route.current.params.manufacturer);
        let manufacturerName = $route.current.params.manufacturer;
        let friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
        let friendlyYear = Util.GetKeyFromUrlFriendly($route.current.params.year);
        let color = Util.GetKeyFromUrlFriendly($route.current.params.color);
        let friendlyColor = Util.GetKeyFromUrlFriendly(color);
        let network = $route.current.params.network;
        let friendlyNetwork = Util.GetKeyFromUrlFriendly(network);
        let issue = $route.current.params.issue;
        let friendlyIssue = Util.GetKeyFromUrlFriendly(issue);
        let model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);

        let contactInfo = stateService.contactInfo || {
                firstName: '',
                lastName: '',
                email: '',
                phone: '',
                leadSource: '',
                password: ''
            }
        $scope.termsChecked = false;
        $scope.hasError = false;
        $scope.errorMessage = '';

        $scope.contactInfo = contactInfo;
        $scope.showTechnicianApproval = false;

        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            },
            {
                subtitle: 'Device',
                title: Util.FriendlyNameForDeviceType(deviceTypeName),
                href: `#/device/`
            },
            {
                subtitle: 'Manufacturer',
                title: friendlyManufacturer,
                href: `#/device/${deviceTypeName}/`
            },
            {
                subtitle: 'Model',
                title: model.model,
                href: `#/device/${deviceTypeName}/${manufacturerName}`
            },
            {
                subtitle: 'Year',
                title: friendlyYear,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}`
            },
            {
                subtitle: 'Color',
                title: friendlyColor,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}`
            },
            {
                subtitle: 'Network',
                title: network,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}/${color}`
            },
            {
                subtitle: 'Issue',
                title: friendlyIssue,
                href: `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}/${color}/${network}`
            }
        ]
        stateService.breadcrumbBackArrowHref = `#/device/${deviceTypeName}/${manufacturerName}/${model.model}/${friendlyYear}/${color}/${network}`;

        $scope.nextClicked = () => {
            if (!contactInfo.firstName) {
                $scope.hasError = true;
                $scope.errorMessage = 'First name is required';
                return;
            }
            if (!contactInfo.lastName) {
                $scope.hasError = true;
                $scope.errorMessage = 'Last name is required';
                return;
            }
            if (!contactInfo.email) {
                $scope.hasError = true;
                $scope.errorMessage = 'Email is required';
                return;
            }
            if (!contactInfo.phone) {
                $scope.hasError = true;
                $scope.errorMessage = 'Phone is required';
                return;
            }
            if (!contactInfo.leadSource) {
                $scope.hasError = true;
                $scope.errorMessage = 'Please share how you heard about us';
                return;
            }
            if (!$scope.termsChecked) {
                $scope.hasError = true;
                $scope.errorMessage = 'Terms of service must be agreed to.';
                return;
            }
            $scope.hasError = false;
            $scope.showTechnicianApproval = true;
        }

        $scope.technicianOKClicked = () => {
            stateService.contactInfo = contactInfo;
            Util.NavigateForward($location, 'submit');
        }

    })

    .controller('SubmitController', function ($scope, $location, stateService, $route, $http) {

        let contactInfo = stateService.contactInfo;
        if (!contactInfo) {
            Util.NavigateBack($location);
        }

        let fields: IField[] = [];

        let deviceTypeRaw = $route.current.params.deviceType;
        let deviceType = Util.FriendlyNameForDeviceType($route.current.params.deviceType);
        let manufacturerName = $route.current.params.manufacturer;
        let friendlyManufacturer = Util.GetKeyFromUrlFriendly($route.current.params.manufacturer);
        let friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
        let year = Util.GetKeyFromUrlFriendly($route.current.params.year);
        let friendlyYear = Util.GetKeyFromUrlFriendly(year);
        let color = Util.GetKeyFromUrlFriendly($route.current.params.color);
        let friendlyColor = Util.GetKeyFromUrlFriendly(color);
        let friendlyIssue = Util.GetKeyFromUrlFriendly($route.current.params.issue);
        let network = Util.GetKeyFromUrlFriendly($route.current.params.network);
        let issue = Util.GetKeyFromUrlFriendly($route.current.params.issue);
        let isIphone = friendlyModel.toLowerCase().indexOf('iphone') > -1;
        let specificManufacturer = $location.search()[Util.SpecificManufacturerSearchQuery];
        let specificModel = $location.search()[Util.SpecificModelSearchQuery];
        let specificYear = $location.search()[Util.SpecificYearSearchQuery];
        let specificColor = $location.search()[Util.SpecificColorSearchQuery];
        let model = Util.GetModel(deviceTypeRaw, friendlyManufacturer, friendlyModel);

        $scope.stateService = stateService;

        stateService.breadcrumbs = [
            {
                subtitle: 'Start',
                title: 'Welcome',
                href: '#/'
            },
            {
                subtitle: 'Device',
                title: deviceType,
                href: `#/device/`
            },
            {
                subtitle: 'Manufacturer',
                title: friendlyManufacturer,
                href: `#/device/${deviceTypeRaw}/`
            },
            {
                subtitle: 'Model',
                title: model.model,
                href: `#/device/${deviceTypeRaw}/${manufacturerName}`
            },
            {
                subtitle: 'Year',
                title: friendlyYear,
                href: `#/device/${deviceTypeRaw}/${manufacturerName}/${model.model}`
            },
            {
                subtitle: 'Color',
                title: friendlyColor,
                href: `#/device/${deviceTypeRaw}/${manufacturerName}/${model.model}/${friendlyYear}`
            },
            {
                subtitle: 'Network',
                title: network,
                href: `#/device/${deviceTypeRaw}/${manufacturerName}/${model.model}/${friendlyYear}/${color}`
            },
            {
                subtitle: 'Issue',
                title: friendlyIssue,
                href: `#/device/${deviceTypeRaw}/${manufacturerName}/${model.model}/${friendlyYear}/${color}/${network}`
            },
            {
                subtitle: 'Contact',
                title: 'Contact Info',
                href: `#/device/${deviceTypeRaw}/${manufacturerName}/${model.model}/${friendlyYear}/${color}/${network}/${issue}`
            }
        ]
        stateService.breadcrumbBackArrowHref = `#/device/${deviceTypeRaw}/${manufacturerName}/${model.model}/${friendlyYear}/${color}/${network}`;

        // Feel free to change 'name' on the fields - that's the display
        // But don't change internalName - that's for the server

        // Don't allow changing of device type
        // fields.push({
        //     name: 'Device Type',
        //     internalName: 'deviceType',
        //     field: deviceType
        // });

        if (friendlyModel === 'Other') {
            // If device model is "Other", replace model name with just the "Specific Model"
            friendlyModel = specificModel;
        } else {
            if (specificModel) {
                // If "Specific Model" exists, then the "Model" Name will be "Model + SpecificModel"
                friendlyModel = friendlyModel + ' ' + specificModel;
            }
        }

        if (year !== Util.NoFieldUrlKey) {
            // If a device has a Year, then make model name: "Model + Year"
            friendlyModel = friendlyModel + ' ' + (specificYear || year);
        }

        fields.push({
            name: 'Manufacturer',
            internalName: 'manufacturer',
            field: specificManufacturer || friendlyManufacturer
        });
        fields.push({
            name: 'Model',
            internalName: 'model',
            field: friendlyModel
        });
        if (year !== Util.NoFieldUrlKey) {
            fields.push({
                name: 'Year',
                internalName: 'year',
                field: specificYear || year
            });
        }
        if (color !== Util.NoFieldUrlKey) {
            fields.push({
                name: 'Color',
                internalName: 'color',
                field: specificColor || color
            });
        }
        if (network !== Util.NoFieldUrlKey) {
            fields.push({
                name: 'Network',
                internalName: 'network',
                field: network
            });
        }
        fields.push({
            name: 'Issue',
            internalName: 'issue',
            field: issue
        });

        let submitForm = {
            fields: fields,
            additionalDamage: '',
            visualInspection: '',
            additionalDetails: '',
            serialOrImei: '',
            technician: '',
            iPhoneQuality: '',
            deviceType: deviceType,
            forceAssetCreation: false
        };

        $scope.submitForm = submitForm;
        $scope.isIphone = isIphone;
        $scope.didSubmit = false;
        $scope.showSuccess = false;
        $scope.showError = false;
        $scope.errorMessage = '';

        let payload = {
            form: submitForm,
            contact: contactInfo
        }
        $scope.submitClicked = () => {
            for (let field of fields) {
                if (!field.field) {
                    $scope.errorMessage = `${field.name} is required`;
                    $scope.showError = true;
                    return;
                }
            }
            if (!submitForm.technician) {
                $scope.errorMessage = `Technician name is required`;
                $scope.showError = true;
                return;
            }
            $scope.showError = false;
            if (!$scope.didSubmit) {
                $scope.didSubmit = true;
                $http.post(API_ENDPOINT, payload).then((response) => {
                    $scope.showError = false;
                    $scope.showSuccess = true;
                    stateService.isComingFromSuccess = true;
                    Util.NavigateHome($location);
                }, (error) => {
                    let message = error.data;
                    if (message === 'DUP_ASSET') {
                        message = 'That asset already exists. Resubmit to use the existing asset, OR enter a new imei / serial number, then resubmit.';
                        $scope.submitForm.forceAssetCreation = true;
                    } else if (message === 'CREATE_CUSTOMER_ERROR') {
                        message = 'RepairShopr Error: customer with this email address is disabled. Please use a different email address and resubmit.';
                    }
                    $scope.errorMessage = message;
                    $scope.didSubmit = false;
                    $scope.showError = true;
                    $scope.showSuccess = false;
                });
            }
        }

    })

interface IField {
    name: string,
    field: string,
    internalName: string
}