/* Campus Mobile Solutions - RepairShopr Authentication Server
 */

import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as request from 'request';
// import Mail = SendGrid.Helpers.Mail.Mail;

// declare const process;
// declare const require;

interface CustomerPayload {
    firstname: string,
    lastname: string,
    email: string,
    mobile: string,
    business_name: string,
    address: string,
    address_2: string,
    city: string,
    state: string,
    zip: string,
    properties: any
}

// interface TicketPayloadProperties {
//     IMEI or Serial Number: string;
//     Password: string;
//     Tested Before By: string;
//     Additional Details: string;
//     Pre-existing Damage: string;
//     Part Quality: string;
// }
// comment_do_not_email: '1' suppresses email from repairshopr. default is '0'
interface TicketPayload {
    subject: string,
    customer_id: string,
    comment_subject: string,
    comment_body: string,
    comment_do_not_email: string,
    problem_type: string,
    properties: any,
    ticket_type_id: string,
    asset_ids: string
}

interface PhoneAssetPayload {
    asset_type_id: string,
    asset_serial: string,
    name: string,
    customer_id: string,
    properties: any;
    //     {
    //     Manufacturer: string,
    //     Model: string,
    //     Color: string,
    //     Network: string,
    //     IMEI: string,
    // };
}
interface TabletAssetPayload {
    asset_type_id: string,
    asset_serial: string,
    name: string,
    customer_id: string,
    properties: any;
    //     {
    //     Manufacturer: string,
    //     Model: string,
    //     'Model Number': string,
    //     Year: string,
    //     Color: string,
    //     Network: string,
    //     IMEI: string,
    // };
}
interface ComputerAssetPayload {
    asset_type_id: string,
    asset_serial: string,
    name: string,
    customer_id: string,
    properties: any;
    //     {
    //     Manufacturer: string,
    //     Model: string,
    //     'Model Number': string,
    //     Year: string,
    //     'Service Tag': string,
    // };
}
interface OtherAssetPayload {
    asset_type_id: string,
    asset_serial: string,
    name: string,
    customer_id: string,
    properties: {
        Manufacturer: string,
        Model: string,
        'Model Number': string,
        Color: string,
    };
}

interface SubmitForm {
    fields: {
        name: string,
        field: string,
        internalName: string
    }[],
    additionalDamage: string,
    visualInspection: string,
    additionalDetails: string,
    serialOrImei: string,
    technician: string,
    iPhoneQuality: string,
    deviceType: string,
    password: string
}

interface Fields {
    deviceType?: string,
    manufacturer?: string,
    model?: string,
    specificModel?: string,
    year?: string,
    color?: string,
    network?: string,
    issue?: string
}

interface MailOpts {
    comment_subject?: string,
    customer_name?: string,
    customer_email?: string,
    customer_phone?: string,
    deviceType?: string,
    manufacturer?: string,
    model?: string,
    specificModel?: string,
    year?: string,
    color?: string,
    network?: string,
    issue?: string,
    imei?: string,
    preexistingConiditions?: string,
    additionalDetails?: string,
    screenQuality?: string,
    ticketId?: string,
    ticketNumber? : string;
}

const PORT = 9000;
const SECRET = process.env.REPAIRSHOPR_API_KEY;
const SENDGRID_API_KEY = process.env.SENDGRID_API_KEY;
const SENDGRID_TEMPLATE_ID = '4b0dc3e2-c23a-4bd8-bebd-3ba879c397a9';
const sendgrid = require('sendgrid');

let app = express();

app.use(function (req: any, res: any, next: any) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'https://www.campusmobilesolutions.com');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

let checkInSummaryEmail = (opts: MailOpts) => {
    console.log('send email started');

    const helper = sendgrid.mail;
    const fromEmail = new helper.Email('cms@campusmobilesolutions.com', 'Campus Mobile Solutions');
    const toEmail = new helper.Email(opts.customer_email);
    const subject = `${opts.manufacturer} ${opts.model} Repair - Ticket #${opts.ticketNumber} (message id: ${opts.ticketId})`;
    const content = new helper.Content('text/html', ' ');
    const mail = new helper.Mail(fromEmail, subject, toEmail, content);
    const category = new helper.Category("check-in-summary");

    let manufacturerField = '';
    let modelField = '';
    let colorField = '';
    let networkField = '';
    let imeiField = '';
    let preexistingConditionsField = '';
    let additionalDetailsField = '';
    let issuesField = '';
    let screenQualityField = '';

    // Device Information Block
    if (opts.manufacturer) {
        manufacturerField = `Manufacturer: ${opts.manufacturer}<br/>`;
    }
    if (opts.model) {
        modelField = `Model: ${opts.model}<br/>`;
    }
    if (opts.color) {
        colorField = `Color: ${opts.color}<br/>`;
    }
    if (opts.network) {
        networkField = `Carrier: ${opts.network}<br/>`
    }
    if (opts.imei) {
        imeiField = `Serial / IMEI: ${opts.imei}<br/>`;
    }
    if (opts.preexistingConiditions) {
        preexistingConditionsField = `Pre-existing Conditions: ${opts.preexistingConiditions}<br/>`;
    }
    if (opts.additionalDetails) {
        additionalDetailsField = `Additional Details: ${opts.additionalDetails}<br/>`
    }

    let deviceInformationBlock =
        `
            ${manufacturerField} 
            ${modelField} 
            ${colorField}
            ${networkField}
            ${imeiField}
            ${preexistingConditionsField}
            ${additionalDetailsField}
        `;

    // Repair Information Block
    if (opts.issue) {
        issuesField = `Issue: ${opts.issue}<br/>`
    }
    if (opts.screenQuality) {
        screenQualityField = `Selected Screen Quality: ${opts.screenQuality}<br/>`;
    }

    let repairInformationBlock =
        `
            ${issuesField}
            ${screenQualityField}
        `

    mail.addCategory(category);

    // Fill in Email Variables
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{device_information}}', deviceInformationBlock));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{repair_information}}', repairInformationBlock));

    // Customer Information block
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{customer_name}}', opts.customer_name));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{phone}}', opts.customer_phone));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{email}}', opts.customer_email));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{ticket_number}}', opts.ticketNumber));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{comment_subject}}', opts.comment_subject));

    // Device and Repair information is now sent as blocks (see deviceInformationBlock and repairInformationBlock)
    // Individual substitutions can be used as variables in email template
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{manufacturer}}', opts.manufacturer));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{model}}', opts.model));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{year}}', opts.year));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{color}}', opts.color));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{network}}', opts.network));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{imei}}', opts.imei));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{preexisting_conditions}}', opts.preexistingConiditions));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{additional_details}}', opts.additionalDetails));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{issue}}', opts.issue));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('{{screen_quality}}', opts.screenQuality));


    mail.setTemplateId(SENDGRID_TEMPLATE_ID);

    return mail.toJSON();
}

let sendEmail = (toSend) => {
    console.log(JSON.stringify(toSend, null, 2));
    //console.log(JSON.stringify(toSend))

    let sg = require('sendgrid')(SENDGRID_API_KEY);

    let requestBody = toSend;
    let emptyRequest = require('sendgrid-rest').request;
    let requestPost = JSON.parse(JSON.stringify(emptyRequest));
    requestPost.method = 'POST';
    requestPost.path = '/v3/mail/send';
    requestPost.body = requestBody;
    sg.API(requestPost, function (error, response) {
        console.log(response.statusCode);
        console.log(response.body);
        console.log(response.headers);
    })
}

let createAsset = (customerId: string, submitForm: SubmitForm, forceAssetCreation: boolean, callback: Function, dupAssetCallback: Function) => {
    console.log('app: begin createAsset()');
    let assetPayload;

    let fields: Fields = {};
    // Generically assign field variables
    for (let field of submitForm.fields) {
        fields[field.internalName] = field.field;
    }

    console.log('app: createAsset fields =\n' + JSON.stringify(fields, null, 4));
    console.log('app: createAsset type = ' + submitForm.deviceType);

    if (submitForm.deviceType === 'Phone') {
        let phonePayload: PhoneAssetPayload = {
            "asset_type_id": "33572",
            "asset_serial": submitForm.serialOrImei,
            "name": fields.manufacturer + ' ' + fields.model,
            "customer_id": customerId,
            "properties": {
                "Manufacturer": fields.manufacturer,
                "Model": fields.model,
                "Color": fields.color,
                "Network": fields.network
            }
        }
        assetPayload = phonePayload;
    } else if (submitForm.deviceType === 'Tablet') {
        let tabletPayload: TabletAssetPayload = {
            "asset_type_id": "33573",
            "asset_serial": submitForm.serialOrImei,
            "name": fields.manufacturer + ' ' + fields.model,
            "customer_id": customerId,
            "properties": {
                "Manufacturer": fields.manufacturer,
                "Model": fields.manufacturer + ' ' + fields.model,
                "Model Number": fields.specificModel,
                "Year": null,
                "Color": fields.color,
                "Network": fields.network
            }
        }
        assetPayload = tabletPayload;
    } else if (submitForm.deviceType === 'Computer') {
        let computerPayload: ComputerAssetPayload = {
            "asset_type_id": "18387",
            "asset_serial": submitForm.serialOrImei,
            "name": fields.manufacturer + ' ' + fields.model,
            "customer_id": customerId,
            "properties": {
                "Manufacturer": fields.manufacturer,
                "Model": fields.model,
                "Model Number": fields.specificModel,
                "Year": fields.year,
                "Service Tag": null
            }
        }
        assetPayload = computerPayload;
    } else {
        let otherPayload: OtherAssetPayload = {
            "asset_type_id": "33931",
            "asset_serial": submitForm.serialOrImei,
            "name": fields.manufacturer + ' ' + fields.model,
            "customer_id": customerId,
            "properties": {
                "Manufacturer": fields.manufacturer,
                "Model": fields.model,
                "Model Number": fields.specificModel,
                "Color": fields.color
            }
        }
        assetPayload = otherPayload;
    }

    console.log('app: assetPayload =\n' + JSON.stringify(assetPayload, null, 4));

    let requestOptions = {
        method: 'POST',
        url: `https://cmsuiuc.repairshopr.com/api/v1/customer_assets?api_key=${SECRET}`,
        json: assetPayload
    }
    console.log('app: create asset');
    console.log('repairshopr: post to /api/v1/customer_assets');
    console.log('repairshopr: requestOptions =\n' + JSON.stringify(requestOptions, null, 4));
    request(requestOptions, (err, httpResponse, body) => {
        console.log('repairshopr: createAsset post response =\n' + JSON.stringify(body, null, 4));
        if (body.success === false && body.message == 'Asset serial has already been taken') {
            let requestOptions = {
                method: 'GET',
                url: `https://cmsuiuc.repairshopr.com/api/v1/customer_assets?api_key=${SECRET}`
            }
            if (forceAssetCreation) {
                console.log('app: since asset exists, get asset id');
                request(requestOptions, (err, httpResponse, body) => {
                    let assets = JSON.parse(body).assets;
                    let assetSerial = submitForm.serialOrImei;
                    console.log('repairshopr: get response: assets list');
                    let foundAsset = null;
                    for (let asset of assets) {
                        if (asset.asset_serial === assetSerial) {
                            foundAsset = asset;
                            break;
                        }
                    }
                    console.log('repairshopr: asset found =\n' + JSON.stringify(foundAsset, null, 4));
                    callback(err, foundAsset);
                });
            } else {
                dupAssetCallback();
            }
        } else {
            console.log('repairshopr: sending body.asset =\n' + JSON.stringify(body.asset, null, 4));
            callback(err, body.asset);
        }
    });
}

let createTicket = (assetId: string, customerId: string, customer: CustomerPayload, submitForm: SubmitForm, callback: Function) => {
    console.log('app: begin createTicket()');

    let ticketTypeId = '';
    let ticketSubject = '';
    let ticketCommentSubject = '';

    let fields: Fields = {};
    // Generically assign field variables
    for (let field of submitForm.fields) {
        fields[field.internalName] = field.field;
    }

    if (submitForm.deviceType === 'Computer') {
        ticketTypeId = '16879';
    } else {
        ticketTypeId = '10508';
    }

    console.log('app: createTicket fields =\n' + JSON.stringify(fields, null, 4));

    if (fields.color) {
        ticketSubject = fields.color + ' ' + fields.manufacturer  + ' ' + fields.model + ' - ' + fields.issue;
    } else {
        ticketSubject = fields.manufacturer + ' ' + fields.model + ' - ' + fields.issue;
    }

    let ticket: TicketPayload = {
        "subject": `${ticketSubject}`,
        "customer_id": customerId,
        "comment_subject": `Initial Issue`,
        "comment_body": `
            ${fields.issue} `,
        "comment_do_not_email": '1',
        "problem_type": fields.issue,
        "properties": {
            "Tested Before By": submitForm.technician,
            "Additional Details": submitForm.additionalDetails,
            "Pre-existing Damage": submitForm.additionalDamage,
            "Part Quality": submitForm.iPhoneQuality,
            "Password": submitForm.password,
            "Visual Inspection": submitForm.visualInspection
        },
        "ticket_type_id": ticketTypeId,
        "asset_ids": assetId
    };
    let requestOptions = {
        method: 'POST',
        url: `https://cmsuiuc.repairshopr.com/api/v1/tickets?api_key=${SECRET}`,
        json: ticket
    };
    console.log('app: create ticket');
    console.log('repairshopr: post to /api/v1/tickets');
    console.log('repairshopr: requestOptions =\n' + JSON.stringify(requestOptions, null, 4));
    request(requestOptions, (err, httpResponse, body) => {
        console.log('repairshopr: createTicket ticket.id = ' + body.ticket.id);
        let mailOpts: MailOpts = {
            comment_subject: ticketCommentSubject,
            customer_name: `${customer.firstname} ${customer.lastname}`,
            customer_email: customer.email,
            customer_phone: customer.mobile,
            deviceType: fields.deviceType,
            manufacturer: fields.manufacturer,
            model: fields.model,
            year: fields.year,
            color: fields.color,
            network: fields.network,
            issue: fields.issue,
            imei: submitForm.serialOrImei,
            preexistingConiditions: submitForm.additionalDamage,
            additionalDetails: submitForm.additionalDetails,
            screenQuality: submitForm.iPhoneQuality,
            ticketId: body.ticket.id.toString(),
            ticketNumber: body.ticket.number.toString()
        }

        sendEmail(checkInSummaryEmail(mailOpts));
        callback(err);
    });
}

app.post('/api/check_in', (req, res) => {

    try {
        console.log('post call started');
        console.log('post: req.body.contact =\n' + JSON.stringify(req.body.contact, null, 4));
        console.log('post: req.body.form =\n' + JSON.stringify(req.body.form, null, 4));

        let body = req.body;
        let contactInfo = req.body.contact;
        let submitForm = req.body.form;
        submitForm.password = contactInfo.password;
        let email = contactInfo.email.toLowerCase().trim();

        let forceAssetCreation = submitForm.forceAssetCreation;

        let customer: CustomerPayload = {
            firstname: contactInfo.firstName,
            lastname: contactInfo.lastName,
            email: email,
            mobile: contactInfo.phone,
            business_name: '',
            address: '',
            address_2: '',
            city: '',
            state: '',
            zip: '',
            properties: {
                'Lead Source': contactInfo.leadSource
            }
        }

        let requestOptions = {
            method: 'POST',
            url: `https://cmsuiuc.repairshopr.com/api/v1/customers?api_key=${SECRET}`,
            json: customer
        }
        request(requestOptions, (err, httpResponse, body) => {
            console.log('repairshopr: post to /api/v1/customers');
            console.log('repairshopr: requestOptions =\n' + JSON.stringify(requestOptions, null, 4));
            if (err) {
                res.status(400).send('There was an error creating the customer.')
            } else {
                if (body.success === false) {
                    console.log('repairshopr: post to /api/v1/customers response: body.success === ' + body.success);
                    console.log('repairshopr: body.message = ' + body.message);
                    let requestOptions = {
                        method: 'GET',
                        url: `https://cmsuiuc.repairshopr.com/api/v1/customers?api_key=${SECRET}`
                    }
                    console.log('app: since customer exists, get customer id');
                    console.log('repairshopr: get from /api/v1/customers');
                    console.log('repairshopr: requestOptions =\n' + JSON.stringify(requestOptions, null, 4))
                    request(requestOptions, (err, httpResponse, body) => {
                        if (err) {
                            res.status(400).send('There was an error retrieving customers.')
                        } else {
                            let customers = JSON.parse(body).customers;
                            console.log('repairshopr: get response: customers list');
                            let foundCustomer = null;
                            for (let customer of customers) {
                                console.log('repairshopr: customer.email = ' + customer.email);
                                console.log('repairshopr: customer.id = ' + customer.id);
                                if (customer.email === email) {
                                    foundCustomer = customer;
                                    console.log('repairshopr: foundCustomer.email/id = ' + foundCustomer.email + '/' + foundCustomer.id);
                                    break;
                                }
                            }

                            if (!foundCustomer) {
                                console.log('repairshopr: customer email exists in repairshopr but could not be retrieved');
                                res.status(400).send('CREATE_CUSTOMER_ERROR');
                            } else {
                                console.log('repairshopr: customer found =\n' + JSON.stringify(foundCustomer, null, 4));
                                let customerId = foundCustomer.id;
                                console.log('repairshopr: customerId: ' + customerId);
                                createAsset(customerId, submitForm, forceAssetCreation,
                                    (err, body) => {
                                        // Success
                                        if (err) {
                                            res.status(400).send('There was an error creating the asset. Customer existed.')
                                        } else {
                                            console.log('app: complete createAsset()');
                                            let assetId = body.id;
                                            createTicket(assetId, customerId, customer, submitForm, (err) => {
                                                if (err) {
                                                    res.status(400).send('There was an error creating the asset. Customer existed.')
                                                } else {
                                                    console.log('app: complete createTicket()');
                                                    res.status(200).send('Success');
                                                }
                                            });
                                        }
                                    },
                                    () => {
                                        // Duplicate asset
                                        res.status(400).send('DUP_ASSET');
                                    }
                                );
                            }
                        }
                    });
                } else {
                    console.log('repairshopr: post to /api/v1/customers response: customer did not exist, created');
                    console.log('repairshopr: new customer =\n' + JSON.stringify(body.customer, null, 4));
                    let customerId = body.customer.id;
                    createAsset(customerId, submitForm, forceAssetCreation,
                        (err, body) => {
                            // Success
                            if (err) {
                                res.status(400).send('There was an error creating the asset. Customer was created.')
                            } else {
                                let assetId = body.id;
                                createTicket(assetId, customerId, customer, submitForm, (err) => {
                                    if (err) {
                                        res.status(400).send('There was an error creating the asset. Customer was created.')
                                    } else {
                                        console.log('res.status.send() from createTicket');
                                        res.status(200).send('Success');
                                    }
                                });
                            }
                        },
                        () => {
                            // Duplicate asset
                            res.status(400).send('DUP_ASSET');
                        }
                    );
                }
            }
        });

        /*
         {
         "firstname": "{{firstname}}",
         "lastname": "{{lastname}}",
         "email": "{{email}}",
         "mobile": "{{mobile}}",
         "business_name": "{{business_name}}",
         "address": "{{address}}",
         "address_2": "{{address_2}}",
         "city": "{{city}}",
         "state": "{{state}}",
         "zip": "{{zip}}"
         }
         */
    } catch (e) {
        res.status(400).send('There was an unknown error: ' + e);
    }
});

app.get('/api/check_in', (req: any, res) => {
    res.json({
        chance: req.chance
    });

    console.log('get: ' + req.url);
});

app.get('/send-test-email-tommy-private', (req: any, res) => {
    console.log('sending email');

    let mailOpts: MailOpts = {
        comment_subject: 'We have your device',
        customer_name: `Tommy Nguyen`,
        customer_email: 'tommy@rcd.cool',
        customer_phone: '1234567890',
        deviceType: 'Laptop',
        manufacturer: 'Apple',
        model: 'MacBook Pro',
        year: '2015',
        color: 'White',
        network: 'Sprint',
        issue: 'Broken Screen',
        imei: Math.floor(Math.random() * 1000000).toString(),
        preexistingConiditions: '',
        additionalDetails: 'Left charger with us',
        screenQuality: '',
        ticketId: 'id' + Math.floor(Math.random() * 10000)
    }

    sendEmail(checkInSummaryEmail(mailOpts));
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send('<html><body><p>Test email sent with the following info</p>' + '<pre>' + JSON.stringify(mailOpts, null, 2) + '</pre></body></html>');
    console.log('200: ' + req.url);
});

app.get('/send-test-email-miko-private', (req: any, res) => {
    console.log('sending email');

    let mailOpts: MailOpts = {
        comment_subject: 'We have your device',
        customer_name: `Miko`,
        customer_email: 'aamikam@gmail.com',
        customer_phone: '1234567890',
        deviceType: 'Laptop',
        manufacturer: 'Apple',
        model: 'MacBook Pro',
        year: '2015',
        color: null,
        network: null,
        issue: 'Broken Screen',
        imei: Math.floor(Math.random() * 1000000).toString(),
        preexistingConiditions: 'Scratched screen',
        additionalDetails: 'Left charger with us',
        screenQuality: 'OEM',
        ticketId: 'id' + Math.floor(Math.random() * 100000)
    }

    sendEmail(checkInSummaryEmail(mailOpts));

    res.setHeader('Content-Type', 'text/html');
    res.status(200).send('<html><body><p>Test email sent with the following info</p>' + '<pre>' + JSON.stringify(mailOpts, null, 2) + '</pre></body></html>');
    console.log('200: ' + req.url);
});

app.get('*', (req, res) => {
    res.status(404).send('You are at the wrong place2.');
    console.log('404: ' + req.url);
});

app.listen(PORT, (err) => {
    if (err) {
        return console.log('server error: ', err);
    }

    console.log('Server listening on: http://localhost:%s', PORT);
});
