# cms-auth-server
A custom authentication server to submit data to RepairShopr API

## App Details

### Instructions for Miko

** Development workflow**:

Before working, let's get the latest code:

```
git pull origin master
```

To deploy client (index.html and main.ts):

```
npm run deploy.client
```

To deploy server (server.ts):

```
npm run deploy.server
```

To update database:

```
npm run update.database
```

To revert files back to "master branch":

``` 
git checkout <filename>
```

When you are sure about your changes, do the following to commit it to Bitbucket repo

```
git add -u
git commit -m "a message goes here"
git push origin master

```

### CMS Auth Server Order of Operations

* Client fills out check-in form
* Technician approves check-in form (in person)
* Technician submits form
* Once submitted:
  * Create Customer using POST /api/v1/customers (see `json/customer-payload.json` for payload)
    * If success, will return an object with customer_id (for use in Ticket Creation)
    * If failure, will return failure message with "success: false"
      * In this case, get all customers with GET /api/v1/customers
        * Filter results by email
        * Return customer_id of matching email
  * With customer_id, now we create Asset and Ticket
  * Create new Asset with POST /api/v1/customer_assets (see `json/asset-*-payload.json`)
  * Create new ticket with POST /api/v1/tickets (see `json/ticket-payload.json` for payload)
    * Customer will get notified via email, handled automatically by RepairShopr
    

### API IDs

**asset_type_id**:

* Phone: 33572
* Computer: 18387
* Tablet: 33573
* Other: 33931

**ticket_type_id**:

* Mobile Device Ticket: 10508
* Computer Ticket: 16879

**Part Quality** (part of ticket creation):

* OEM: 48124
* Premium: 48125
* Premium Value: 48126

### Mock DB

We use mockdb.json for device database. In general:

* There are 4 repairTypes: phones, tablets, computers, other
* Each repairType has an array of networks, array of issues, and array of manufacturers
* Each manufacturer has a name and array of models
* Each model has a name and info object
* Each info object has an array of colors and an array of model-specific issues
* Each color has a name and subtitle

### Check-in Form

* First Screen: - Customer will click here to get started

* Second Screen: - Customer will see four options:

  1. Phones -> Manufacturers (include Apple, Samsung, Google, etc) -> Models -> Color -> Network -> Issues
  2. Tablets -> Manufacturers (include Apple, Samsung, Google, etc) -> Models -> Color -> Network or WiFi -> Issues
  3. Computers -> Manufacturers -> If Apple, list all Mac models. If not Apple, list general models then prompt for specific model with text field -> Issues
  4. Other -> iPods and Others -> Color -> Issues

* Third Screen: - Customer Contact Info: First, Last name, email (required), phone number, Terms of Service & Signature

* Fourth Screen: 
  - Thank you and Find a CMS Technician
  - Technician will review a summary
  - Have ability to edit all fields with text fields
  - additional damage
  - visual inspection, ie works or doesn't work
  - additional notes
  - Serial # or IMEI
  - For iPhones, select quality of parts: OEM, Premium Value, and Premium
  - Technician's name
  - Submit
  
## Sendgrid Email Information

* cms-auth-server uses Sendgrid's Template API for sending emails
* Templates can be found: https://sendgrid.com/templates
* Each Template has an ID. For example, "Check-in Summary" has `4b0dc3e2-c23a-4bd8-bebd-3ba879c397a9`
* Each Template can have multiple versions, with one active at a time
* Modifications to email Template should be done inside Sendgrid
* CSS should be done in-line to support all browsers and email clients

## Prerequisites 

* Install `ansible-playbook`
* Install `nodejs` v6 or higher
* Install `npm` version 3
* Install `node-sass` npm package globally 

## Installation and deployment 

Clone: `git clone git@bitbucket.org:rightclickdesign/cms-auth-server.git`

**Deploy client**: `npm run deploy.client` or `./deploy/ftp-deploy`

**Update database-json.js**: `npm run update.database` or `./deploy/ftp-update-database`

**Deploy server**: `npm run deploy`

**To deploy without password prompt:**

1. Create txt file called `config/vault.txt`
2. Add a single line with the deploy code in txt file
3. Deploy with `npm run deployvp`

Note: `vault.txt` is in ignore file and should not be committed to codebase

## Configuration 

### TODO Notes

1. cms.rcd.cool
2. nodejs
3. letsencrypt
4. 104.131.167.77 on DigitalOcean (an RCD-owned droplet)
5. WordPress 

### WordPress Configuration

* Page Template file: `wp-content/`

### Most of the configuration happens in the `vars` directory. 

1. `vars/default.yml`: contains most of variables and preferences.
2. `vars/prod-config.yml`: contains production secrets, such as your AWS Token. This file is password protected using `ansible-vault`. The example vault's password is 'awesome'. 
3. `vars/staging-config.yml`: contains staging secrets. This file contains the exact same variables as `prod-config.yml`.

### Inventory files 

1. `production`: a list of production servers.
2. `staging`: a list of staging servers.

To make life a little easier, add the hosts to your `~/.ssh/config` file. See `config/ssh_config` for host information.

Have fun!