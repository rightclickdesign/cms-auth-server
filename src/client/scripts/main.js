// nginx -> node proxy
var API_ENDPOINT = 'https://cms.rcd.cool/api/check_in';
//const API_ENDPOINT = 'http://localhost:9000/api';
var Util = (function () {
    function Util() {
    }
    Util.GetUrlFriendlyKey = function (str) {
        str = str.replace(new RegExp(' ', 'g'), '-');
        str = str.replace(new RegExp('/', 'g'), '_');
        return str;
    };
    Util.GetKeyFromUrlFriendly = function (str) {
        var key = str.replace(new RegExp('-', 'g'), ' ');
        key = key.replace(new RegExp('_', 'g'), '/');
        return key;
    };
    Util.GetDeviceType = function (deviceType) {
        for (var _i = 0, _a = REPAIR_TYPES.repairTypes; _i < _a.length; _i++) {
            var type = _a[_i];
            for (var key in type) {
                if (key === deviceType) {
                    return type[key];
                }
            }
        }
    };
    Util.FriendlyNameForDeviceType = function (deviceType) {
        switch (deviceType) {
            case 'phone':
                return 'Phone';
            case 'tablet':
                return 'Tablet';
            case 'computer':
                return 'Computer';
            case 'other':
                return 'Other';
        }
    };
    Util.GetManufacturersForDeviceType = function (deviceType) {
        return Util.GetDeviceType(deviceType).manufacturers;
    };
    Util.GetModels = function (deviceType, manufacturerName) {
        var manufacturers = Util.GetManufacturersForDeviceType(deviceType);
        for (var _i = 0, manufacturers_1 = manufacturers; _i < manufacturers_1.length; _i++) {
            var manufacturer = manufacturers_1[_i];
            if (manufacturer.name === manufacturerName) {
                return manufacturer.models;
            }
        }
    };
    Util.GetModel = function (deviceType, manufacturerName, modelName) {
        var manufacturers = Util.GetManufacturersForDeviceType(deviceType);
        var models = Util.GetModels(deviceType, manufacturerName);
        for (var _i = 0, models_1 = models; _i < models_1.length; _i++) {
            var model = models_1[_i];
            if (model.model === modelName) {
                return model;
            }
        }
    };
    Util.NavigateForward = function ($location, key, numToSkip) {
        var pathKey = Util.GetUrlFriendlyKey(key);
        var path = $location.path() + ("/" + pathKey);
        if (numToSkip) {
            for (var i = 0; i < numToSkip; i++) {
                path += "/" + Util.NoFieldUrlKey;
            }
        }
        $location.path(path);
    };
    Util.NavigateBack = function ($location) {
        var path = $location.path().substring(0, $location.path().lastIndexOf('/'));
        $location.path(path);
    };
    Util.NavigateHome = function ($location) {
        $location.path('');
    };
    return Util;
}());
Util.NoFieldUrlKey = 'n';
Util.SpecificModelSearchQuery = 'specificModel';
Util.SpecificYearSearchQuery = 'specificYear';
Util.SpecificColorSearchQuery = 'specificColor';
Util.SpecificManufacturerSearchQuery = 'specificManufacturer';
angular.module('CMSApp', ['ngRoute'])
    .config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
        templateUrl: 'hello.html',
        controller: 'HelloController'
    })
        .when('/device', {
        templateUrl: 'device-start.html',
        controller: 'DeviceController'
    })
        .when('/device/:deviceType', {
        templateUrl: 'manufacturers.html',
        controller: 'ManufacturerController'
    })
        .when('/device/:deviceType/:manufacturer', {
        templateUrl: 'models.html',
        controller: 'ModelsController'
    })
        .when('/device/:deviceType/:manufacturer/:model', {
        templateUrl: 'years.html',
        controller: 'YearsController'
    })
        .when('/device/:deviceType/:manufacturer/:model/:year', {
        templateUrl: 'colors.html',
        controller: 'ColorsController'
    })
        .when('/device/:deviceType/:manufacturer/:model/:year/:color', {
        templateUrl: 'networks.html',
        controller: 'NetworkController'
    })
        .when('/device/:deviceType/:manufacturer/:model/:year/:color/:network', {
        templateUrl: 'issues.html',
        controller: 'IssuesController'
    })
        .when('/device/:deviceType/:manufacturer/:model/:year/:color/:network/:issue', {
        templateUrl: 'contact.html',
        controller: 'ContactController'
    })
        .when('/device/:deviceType/:manufacturer/:model/:year/:color/:network/:issue/:submit', {
        templateUrl: 'submit.html',
        controller: 'SubmitController'
    });
    // use the HTML5 History API
    $locationProvider.html5Mode(false).hashPrefix('');
})
    .service('stateService', function () {
    this.contactInfo = null;
    this.isComingFromSuccess = false;
    this.breadcrumbBackArrowHref = '#/';
    this.breadcrumbs = [];
})
    .controller('MainController', function ($scope, $location, stateService) {
    $scope.$on('$locationChangeStart', function (event) {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
    $scope.stateService = stateService;
    $scope.NoFieldUrlKey = Util.NoFieldUrlKey;
})
    .controller('HelloController', function ($scope, $location, stateService) {
    stateService.contactInfo = null;
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        }
    ];
    stateService.breadcrumbBackArrowHref = '#/';
    if (stateService.isComingFromSuccess) {
        $scope.showSuccess = true;
        setTimeout(function () {
            $scope.showSuccess = false;
            $scope.$apply();
        });
    }
    $scope.startClicked = function () {
        $location.path('/device');
    };
})
    .controller('DeviceController', function ($scope, $location, stateService) {
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        },
        {
            subtitle: 'Device',
            title: 'Choosing',
            href: '#/device'
        }
    ];
    stateService.breadcrumbBackArrowHref = '#/';
    $scope.showSpecificModelPrompt = false;
    $scope.showSpecificModelError = false;
    var selectedChoice;
    $scope.choices = ['Phone', 'Tablet', 'Computer', 'Other'];
    $scope.choiceClicked = function (choice) {
        var choicePath = choice.toLowerCase();
        $location.path("/device/" + choicePath);
    };
})
    .controller('ManufacturerController', function ($scope, $location, stateService, $route) {
    var deviceType = $route.current.params.deviceType;
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        },
        {
            subtitle: 'Device',
            title: Util.FriendlyNameForDeviceType(deviceType),
            href: "#/device/"
        },
        {
            subtitle: 'Manufacturer',
            title: 'Choosing',
            href: "#/device/" + deviceType
        }
    ];
    stateService.breadcrumbBackArrowHref = '#/device/';
    var manufacturers = Util.GetManufacturersForDeviceType(deviceType);
    for (var i = 0; i < manufacturers.length; i++) {
        if (!manufacturers[i].name) {
            manufacturers.splice(i, 1);
        }
    }
    $scope.deviceTypeName = Util.FriendlyNameForDeviceType(deviceType) !== 'Other' ? Util.FriendlyNameForDeviceType(deviceType) : '';
    $scope.manufacturers = manufacturers;
    $scope.showSpecificModelPrompt = false;
    $scope.showSpecificModelError = false;
    var selectedManufacturer;
    $scope.manufacturerClicked = function (manufacturer) {
        //console.log(manufacturer);
        if (manufacturer.name === 'Other') {
            selectedManufacturer = manufacturer;
            $scope.showSpecificModelPrompt = true;
            setTimeout(function () {
                document.getElementById('specificModelInput').focus();
            }, 20);
        }
        else {
            Util.NavigateForward($location, manufacturer.name);
        }
    };
    $scope.nextClicked = function () {
        if ($scope.specificModel) {
            $location.search(Util.SpecificManufacturerSearchQuery, $scope.specificModel);
            Util.NavigateForward($location, selectedManufacturer.name);
        }
        else {
            $scope.showSpecificModelError = true;
        }
    };
})
    .controller('ModelsController', function ($scope, $location, stateService, $route) {
    var deviceTypeName = $route.current.params.deviceType;
    var manufacturer = $route.current.params.manufacturer;
    var friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturer);
    var models = Util.GetModels(deviceTypeName, friendlyManufacturer);
    for (var _i = 0, models_2 = models; _i < models_2.length; _i++) {
        var model = models_2[_i];
        model.activated = false;
    }
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        },
        {
            subtitle: 'Device',
            title: Util.FriendlyNameForDeviceType(deviceTypeName),
            href: "#/device/"
        },
        {
            subtitle: 'Manufacturer',
            title: friendlyManufacturer,
            href: "#/device/" + deviceTypeName + "/"
        },
        {
            subtitle: 'Model',
            title: 'Choosing',
            href: "#/device/" + deviceTypeName + "/" + manufacturer
        }
    ];
    stateService.breadcrumbBackArrowHref = "#/device/" + deviceTypeName + "/";
    $scope.selectedModel = null;
    $scope.models = models;
    $scope.showSpecificModelPrompt = false;
    $scope.specificModel = '';
    $scope.deviceTypeName = Util.FriendlyNameForDeviceType(deviceTypeName) !== 'Other' ? Util.FriendlyNameForDeviceType(deviceTypeName) : '';
    $scope.manufacturerName = friendlyManufacturer !== 'Other' ? friendlyManufacturer : '';
    var doNavigate = function () {
        // Check to see if we do have year and / or colors and / or network.
        // If no year or color or network, jump to Issues
        // If no year or color, jump to Network.
        // If no year but color, jump to Color.
        // Else go forward
        var deviceType = Util.GetDeviceType(deviceTypeName);
        var hasYear = $scope.selectedModel.info && $scope.selectedModel.info.year;
        var hasColors = $scope.selectedModel.info && $scope.selectedModel.info.colors;
        var hasNetwork = deviceType.networks;
        if (!hasYear && !hasColors && !hasNetwork) {
            Util.NavigateForward($location, $scope.selectedModel.model, 3);
        }
        else if (!hasYear && !hasColors) {
            Util.NavigateForward($location, $scope.selectedModel.model, 2);
        }
        else if (!hasYear) {
            Util.NavigateForward($location, $scope.selectedModel.model, 1);
        }
        else {
            Util.NavigateForward($location, $scope.selectedModel.model);
        }
    };
    $scope.modelClicked = function (model) {
        // OLD: If Apple, list all Mac models. If not Apple, list general models then prompt for specific model with text field
        $scope.selectedModel = model;
        for (var _i = 0, _a = $scope.models; _i < _a.length; _i++) {
            var model_1 = _a[_i];
            model_1.activated = false;
        }
        model.activated = true;
        // if (deviceTypeName === 'computer') {
        //     let isApple = friendlyManufacturer === 'Apple';
        //     if (!isApple) {
        //         $scope.showSpecificModelPrompt = true;
        //         setTimeout(() => {
        //             document.getElementById('specificModelInput').focus();
        //         }, 20);
        //     } else {
        //         doNavigate();
        //     }
        // } else
        if (model.model === 'Other' || (model.info && model.info['model #'])) {
            $scope.showSpecificModelPrompt = true;
            setTimeout(function () {
                document.getElementById('specificModelInput').focus();
            }, 20);
        }
        else {
            doNavigate();
        }
    };
    $scope.nextClicked = function () {
        if ($scope.specificModel) {
            $location.search(Util.SpecificModelSearchQuery, $scope.specificModel);
            doNavigate();
        }
        else {
            $scope.showSpecificModelError = true;
        }
    };
})
    .controller('YearsController', function ($scope, $location, stateService, $route) {
    var deviceTypeName = $route.current.params.deviceType;
    var manufacturerName = $route.current.params.manufacturer;
    var friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturerName);
    var friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
    var model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);
    // Check if years exists first. if not, navigate back one more time
    var hasYear = model.info && model.info.year;
    if (!hasYear) {
        Util.NavigateBack($location);
    }
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        },
        {
            subtitle: 'Device',
            title: Util.FriendlyNameForDeviceType(deviceTypeName),
            href: "#/device/"
        },
        {
            subtitle: 'Manufacturer',
            title: friendlyManufacturer,
            href: "#/device/" + deviceTypeName + "/"
        },
        {
            subtitle: 'Model',
            title: model.model,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName
        },
        {
            subtitle: 'Year',
            title: 'Choosing',
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model
        }
    ];
    stateService.breadcrumbBackArrowHref = "#/device/" + deviceTypeName + "/" + manufacturerName;
    $scope.deviceTypeName = Util.FriendlyNameForDeviceType(deviceTypeName);
    $scope.years = model.info.year;
    $scope.showSpecificModelPrompt = false;
    $scope.showSpecificModelError = false;
    var selectedYear;
    var doNavigate = function (theYear) {
        // Check to see if we do have colors and / or network.
        // If no colors or network, jump to Issues
        // If no colors but network, jump to Network.
        // Else go forward
        var deviceType = Util.GetDeviceType(deviceTypeName);
        var hasColors = model.info && model.info.colors;
        var hasNetwork = deviceType.networks;
        if (!hasColors && !hasNetwork) {
            Util.NavigateForward($location, theYear.name, 2);
        }
        else if (!hasColors) {
            Util.NavigateForward($location, theYear.name, 1);
        }
        else {
            Util.NavigateForward($location, theYear.name);
        }
    };
    $scope.yearClicked = function (year) {
        if (year.name === 'Other') {
            selectedYear = year;
            $scope.showSpecificYearPrompt = true;
            setTimeout(function () {
                document.getElementById('specificYearInput').focus();
            }, 20);
        }
        else {
            doNavigate(year);
        }
    };
    $scope.nextClicked = function () {
        if ($scope.specificYear) {
            $location.search(Util.SpecificYearSearchQuery, $scope.specificYear);
            doNavigate(selectedYear);
        }
        else {
            $scope.showSpecificYearError = true;
        }
    };
})
    .controller('ColorsController', function ($scope, $location, stateService, $route) {
    var deviceTypeName = $route.current.params.deviceType;
    var manufacturerName = $route.current.params.manufacturer;
    var friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturerName);
    var friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
    var friendlyYear = Util.GetKeyFromUrlFriendly($route.current.params.year);
    var model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);
    // Check if years colors exists first. if not, navigate back one more time
    var hasColors = model.info && model.info.colors;
    if (!hasColors) {
        Util.NavigateBack($location);
    }
    stateService.comingFromField = 'color';
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        },
        {
            subtitle: 'Device',
            title: Util.FriendlyNameForDeviceType(deviceTypeName),
            href: "#/device/"
        },
        {
            subtitle: 'Manufacturer',
            title: friendlyManufacturer,
            href: "#/device/" + deviceTypeName + "/"
        },
        {
            subtitle: 'Model',
            title: model.model,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName
        },
        {
            subtitle: 'Year',
            title: friendlyYear,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model
        },
        {
            subtitle: 'Color',
            title: 'Choosing',
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear
        }
    ];
    stateService.breadcrumbBackArrowHref = "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model;
    $scope.colors = model.info.colors;
    $scope.showSpecificModelPrompt = false;
    $scope.showSpecificModelError = false;
    var selectedColor;
    var doNavigate = function (theColor) {
        // Check to see if we do have a network.
        // If no network, skip.
        var deviceType = Util.GetDeviceType(deviceTypeName);
        var hasNetwork = deviceType.networks;
        if (!hasNetwork) {
            Util.NavigateForward($location, theColor.name, 1);
        }
        else {
            Util.NavigateForward($location, theColor.name);
        }
    };
    $scope.colorClicked = function (color) {
        if (color.name === 'Other') {
            selectedColor = color;
            $scope.showSpecificModelPrompt = true;
            setTimeout(function () {
                document.getElementById('specificModelInput').focus();
            }, 20);
        }
        else {
            doNavigate(color);
        }
    };
    $scope.nextClicked = function () {
        if ($scope.specificModel) {
            $location.search(Util.SpecificColorSearchQuery, $scope.specificModel);
            doNavigate(selectedColor);
        }
        else {
            $scope.showSpecificModelError = true;
        }
    };
})
    .controller('NetworkController', function ($scope, $location, stateService, $route) {
    var deviceTypeName = $route.current.params.deviceType;
    var deviceType = Util.GetDeviceType(deviceTypeName);
    var manufacturerName = $route.current.params.manufacturer;
    var friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturerName);
    var friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
    var friendlyYear = Util.GetKeyFromUrlFriendly($route.current.params.year);
    var friendlyColor = Util.GetKeyFromUrlFriendly($route.current.params.color);
    var model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);
    var networks = deviceType.networks;
    // Check if years networks exists first. if not, navigate back one more time
    if (!networks) {
        Util.NavigateBack($location);
    }
    stateService.comingFromField = 'network';
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        },
        {
            subtitle: 'Device',
            title: Util.FriendlyNameForDeviceType(deviceTypeName),
            href: "#/device/"
        },
        {
            subtitle: 'Manufacturer',
            title: friendlyManufacturer,
            href: "#/device/" + deviceTypeName + "/"
        },
        {
            subtitle: 'Model',
            title: model.model,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName
        },
        {
            subtitle: 'Year',
            title: friendlyYear,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model
        },
        {
            subtitle: 'Color',
            title: friendlyColor,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear
        },
        {
            subtitle: 'Network',
            title: 'Choosing',
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + friendlyColor
        }
    ];
    stateService.breadcrumbBackArrowHref = "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear;
    $scope.networks = networks;
    $scope.networkClicked = function (network) {
        Util.NavigateForward($location, network);
    };
})
    .controller('IssuesController', function ($scope, $location, stateService, $route) {
    var deviceTypeName = $route.current.params.deviceType;
    var deviceType = Util.GetDeviceType(deviceTypeName);
    var issues = deviceType.issues;
    var manufacturerName = $route.current.params.manufacturer;
    var friendlyManufacturer = Util.GetKeyFromUrlFriendly(manufacturerName);
    var friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
    var friendlyYear = Util.GetKeyFromUrlFriendly($route.current.params.year);
    var color = $route.current.params.color;
    var friendlyColor = Util.GetKeyFromUrlFriendly(color);
    var network = $route.current.params.network;
    var model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        },
        {
            subtitle: 'Device',
            title: Util.FriendlyNameForDeviceType(deviceTypeName),
            href: "#/device/"
        },
        {
            subtitle: 'Manufacturer',
            title: friendlyManufacturer,
            href: "#/device/" + deviceTypeName + "/"
        },
        {
            subtitle: 'Model',
            title: model.model,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName
        },
        {
            subtitle: 'Year',
            title: friendlyYear,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model
        },
        {
            subtitle: 'Color',
            title: friendlyColor,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear
        },
        {
            subtitle: 'Network',
            title: network,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color
        },
        {
            subtitle: 'Issue',
            title: 'Choosing',
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color + "/" + network
        }
    ];
    stateService.breadcrumbBackArrowHref = "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color;
    if (model.info && model.info.issue) {
        var issuesCombined = issues.slice(0);
        var index = issuesCombined.length;
        issuesCombined.splice.apply(issuesCombined, [index - 1, 0].concat(model.info.issue));
        $scope.issues = issuesCombined;
    }
    else {
        $scope.issues = issues;
    }
    $scope.issueClicked = function (issue) {
        Util.NavigateForward($location, issue);
    };
})
    .controller('ContactController', function ($scope, $location, stateService, $route) {
    var deviceTypeName = $route.current.params.deviceType;
    var friendlyManufacturer = Util.GetKeyFromUrlFriendly($route.current.params.manufacturer);
    var manufacturerName = $route.current.params.manufacturer;
    var friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
    var friendlyYear = Util.GetKeyFromUrlFriendly($route.current.params.year);
    var color = Util.GetKeyFromUrlFriendly($route.current.params.color);
    var friendlyColor = Util.GetKeyFromUrlFriendly(color);
    var network = $route.current.params.network;
    var friendlyNetwork = Util.GetKeyFromUrlFriendly(network);
    var issue = $route.current.params.issue;
    var friendlyIssue = Util.GetKeyFromUrlFriendly(issue);
    var model = Util.GetModel(deviceTypeName, friendlyManufacturer, friendlyModel);
    var contactInfo = stateService.contactInfo || {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        leadSource: '',
        password: ''
    };
    $scope.termsChecked = false;
    $scope.hasError = false;
    $scope.errorMessage = '';
    $scope.contactInfo = contactInfo;
    $scope.showTechnicianApproval = false;
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        },
        {
            subtitle: 'Device',
            title: Util.FriendlyNameForDeviceType(deviceTypeName),
            href: "#/device/"
        },
        {
            subtitle: 'Manufacturer',
            title: friendlyManufacturer,
            href: "#/device/" + deviceTypeName + "/"
        },
        {
            subtitle: 'Model',
            title: model.model,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName
        },
        {
            subtitle: 'Year',
            title: friendlyYear,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model
        },
        {
            subtitle: 'Color',
            title: friendlyColor,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear
        },
        {
            subtitle: 'Network',
            title: network,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color
        },
        {
            subtitle: 'Issue',
            title: friendlyIssue,
            href: "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color + "/" + network
        }
    ];
    stateService.breadcrumbBackArrowHref = "#/device/" + deviceTypeName + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color + "/" + network;
    $scope.nextClicked = function () {
        if (!contactInfo.firstName) {
            $scope.hasError = true;
            $scope.errorMessage = 'First name is required';
            return;
        }
        if (!contactInfo.lastName) {
            $scope.hasError = true;
            $scope.errorMessage = 'Last name is required';
            return;
        }
        if (!contactInfo.email) {
            $scope.hasError = true;
            $scope.errorMessage = 'Email is required';
            return;
        }
        if (!contactInfo.phone) {
            $scope.hasError = true;
            $scope.errorMessage = 'Phone is required';
            return;
        }
        if (!contactInfo.leadSource) {
            $scope.hasError = true;
            $scope.errorMessage = 'Please share how you heard about us';
            return;
        }
        if (!$scope.termsChecked) {
            $scope.hasError = true;
            $scope.errorMessage = 'Terms of service must be agreed to.';
            return;
        }
        $scope.hasError = false;
        $scope.showTechnicianApproval = true;
    };
    $scope.technicianOKClicked = function () {
        stateService.contactInfo = contactInfo;
        Util.NavigateForward($location, 'submit');
    };
})
    .controller('SubmitController', function ($scope, $location, stateService, $route, $http) {
    var contactInfo = stateService.contactInfo;
    if (!contactInfo) {
        Util.NavigateBack($location);
    }
    var fields = [];
    var deviceTypeRaw = $route.current.params.deviceType;
    var deviceType = Util.FriendlyNameForDeviceType($route.current.params.deviceType);
    var manufacturerName = $route.current.params.manufacturer;
    var friendlyManufacturer = Util.GetKeyFromUrlFriendly($route.current.params.manufacturer);
    var friendlyModel = Util.GetKeyFromUrlFriendly($route.current.params.model);
    var year = Util.GetKeyFromUrlFriendly($route.current.params.year);
    var friendlyYear = Util.GetKeyFromUrlFriendly(year);
    var color = Util.GetKeyFromUrlFriendly($route.current.params.color);
    var friendlyColor = Util.GetKeyFromUrlFriendly(color);
    var friendlyIssue = Util.GetKeyFromUrlFriendly($route.current.params.issue);
    var network = Util.GetKeyFromUrlFriendly($route.current.params.network);
    var issue = Util.GetKeyFromUrlFriendly($route.current.params.issue);
    var isIphone = friendlyModel.toLowerCase().indexOf('iphone') > -1;
    var specificManufacturer = $location.search()[Util.SpecificManufacturerSearchQuery];
    var specificModel = $location.search()[Util.SpecificModelSearchQuery];
    var specificYear = $location.search()[Util.SpecificYearSearchQuery];
    var specificColor = $location.search()[Util.SpecificColorSearchQuery];
    var model = Util.GetModel(deviceTypeRaw, friendlyManufacturer, friendlyModel);
    $scope.stateService = stateService;
    stateService.breadcrumbs = [
        {
            subtitle: 'Start',
            title: 'Welcome',
            href: '#/'
        },
        {
            subtitle: 'Device',
            title: deviceType,
            href: "#/device/"
        },
        {
            subtitle: 'Manufacturer',
            title: friendlyManufacturer,
            href: "#/device/" + deviceTypeRaw + "/"
        },
        {
            subtitle: 'Model',
            title: model.model,
            href: "#/device/" + deviceTypeRaw + "/" + manufacturerName
        },
        {
            subtitle: 'Year',
            title: friendlyYear,
            href: "#/device/" + deviceTypeRaw + "/" + manufacturerName + "/" + model.model
        },
        {
            subtitle: 'Color',
            title: friendlyColor,
            href: "#/device/" + deviceTypeRaw + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear
        },
        {
            subtitle: 'Network',
            title: network,
            href: "#/device/" + deviceTypeRaw + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color
        },
        {
            subtitle: 'Issue',
            title: friendlyIssue,
            href: "#/device/" + deviceTypeRaw + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color + "/" + network
        },
        {
            subtitle: 'Contact',
            title: 'Contact Info',
            href: "#/device/" + deviceTypeRaw + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color + "/" + network + "/" + issue
        }
    ];
    stateService.breadcrumbBackArrowHref = "#/device/" + deviceTypeRaw + "/" + manufacturerName + "/" + model.model + "/" + friendlyYear + "/" + color + "/" + network;
    // Feel free to change 'name' on the fields - that's the display
    // But don't change internalName - that's for the server
    // Don't allow changing of device type
    // fields.push({
    //     name: 'Device Type',
    //     internalName: 'deviceType',
    //     field: deviceType
    // });
    if (friendlyModel === 'Other') {
        // If device model is "Other", replace model name with just the "Specific Model"
        friendlyModel = specificModel;
    }
    else {
        if (specificModel) {
            // If "Specific Model" exists, then the "Model" Name will be "Model + SpecificModel"
            friendlyModel = friendlyModel + ' ' + specificModel;
        }
    }
    if (year !== Util.NoFieldUrlKey) {
        // If a device has a Year, then make model name: "Model + Year"
        friendlyModel = friendlyModel + ' ' + (specificYear || year);
    }
    fields.push({
        name: 'Manufacturer',
        internalName: 'manufacturer',
        field: specificManufacturer || friendlyManufacturer
    });
    fields.push({
        name: 'Model',
        internalName: 'model',
        field: friendlyModel
    });
    if (year !== Util.NoFieldUrlKey) {
        fields.push({
            name: 'Year',
            internalName: 'year',
            field: specificYear || year
        });
    }
    if (color !== Util.NoFieldUrlKey) {
        fields.push({
            name: 'Color',
            internalName: 'color',
            field: specificColor || color
        });
    }
    if (network !== Util.NoFieldUrlKey) {
        fields.push({
            name: 'Network',
            internalName: 'network',
            field: network
        });
    }
    fields.push({
        name: 'Issue',
        internalName: 'issue',
        field: issue
    });
    var submitForm = {
        fields: fields,
        additionalDamage: '',
        visualInspection: '',
        additionalDetails: '',
        serialOrImei: '',
        technician: '',
        iPhoneQuality: '',
        deviceType: deviceType,
        forceAssetCreation: false
    };
    $scope.submitForm = submitForm;
    $scope.isIphone = isIphone;
    $scope.didSubmit = false;
    $scope.showSuccess = false;
    $scope.showError = false;
    $scope.errorMessage = '';
    var payload = {
        form: submitForm,
        contact: contactInfo
    };
    $scope.submitClicked = function () {
        for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++) {
            var field = fields_1[_i];
            if (!field.field) {
                $scope.errorMessage = field.name + " is required";
                $scope.showError = true;
                return;
            }
        }
        if (!submitForm.technician) {
            $scope.errorMessage = "Technician name is required";
            $scope.showError = true;
            return;
        }
        $scope.showError = false;
        if (!$scope.didSubmit) {
            $scope.didSubmit = true;
            $http.post(API_ENDPOINT, payload).then(function (response) {
                $scope.showError = false;
                $scope.showSuccess = true;
                stateService.isComingFromSuccess = true;
                Util.NavigateHome($location);
            }, function (error) {
                var message = error.data;
                if (message === 'DUP_ASSET') {
                    message = 'That asset already exists. Resubmit to use the existing asset, OR enter a new imei / serial number, then resubmit.';
                    $scope.submitForm.forceAssetCreation = true;
                }
                else if (message === 'CREATE_CUSTOMER_ERROR') {
                    message = 'RepairShopr Error: customer with this email address is disabled. Please use a different email address and resubmit.';
                }
                $scope.errorMessage = message;
                $scope.didSubmit = false;
                $scope.showError = true;
                $scope.showSuccess = false;
            });
        }
    };
});
